var socket, userLeague, serverArr;
$(function() {
  console.log('Testing...');

  userLeague = 'league2';
  username = 'alex1';

  socket = io.connect();

  console.log('test...')
  socket.emit('USER_CONNECT', {username: username, league: userLeague});

  socket.on('USER_CONNECTED', function (data) {
    console.log(data);
    serverArr = data.onlineClientInfoByLeague;
  });

  // socket.emit('LOOK_FOR_BATTLE', {league: userLeague, socketID: socket.id})

  // socket.emit('START_TRAINING', { username: username });

  socket.emit('ATTEMPT_LOGIN', {username: username});

  socket.on('LOGIN_SUCCESS', function(data) {
    console.log('LOGIN_SUCCESS --> ' + JSON.stringify(data));
  });

  socket.on('LOGIN_FAILED', function(data) {
    console.log('LOGIN_FAILED ');
  });

  socket.on('TRAINING_RESULTS', function(data) {
    console.log('TRAINING_RESULTS--> msg: ' + data.msg + ' username: ' + data.username);
  });

  socket.on('LEAGUE_EMPTY', function () {
    console.log('League is empty');
  });

  socket.on('CONFIRM_BATTLE', function (data) {
    console.log('CONFIRM_BATTLE: ' + JSON.stringify(data));
  });
})
