const path                        = require('path');
const express                     = require('express');
const _                           = require('underscore');
const mongoose                    = require('mongoose');
const { User, Player, testUser }  = require('./dbSchemas');
const Battle                      = require('./BattleCode');

const app       = express();
const server    = require('http').Server(app);
const io        = require('socket.io')(server);

const port = process.env.PORT || 8080;

mongoose.connect('mongodb://alexald:28255@ds153637.mlab.com:53637/baseball-manager');

app.use(express.static(path.join(__dirname, '../static')));

// TESTING THE DB
// User.find({}, 'username exp', function (err, users) {
//   console.info(JSON.stringify(_.sortBy(users, 'exp')));
// });
//
// User.find({}, function(err, users){
//   console.info(users);
//   for (var i = 0; i < users.length; i++) {
//     users[i].isOnline = false;
//     users[i].socketId = 0;
//     users[i].save();
//   }
// });

var basket = {};

const FirstLeague = 'Pee Wee League';
const InningNames = ['inning1', 'inning2', 'inning3', 'inning4', 'inning5', 'inning6', 'inning7', 'inning8', 'inning9', 'total'];

io.on('connection', function (socket) {
  // keep track of the socket user's league and username
  var userLeague, username;

  // Client Call: Attempt to login in with provided username
  socket.on('ATTEMPT_LOGIN', function(data) {
    // Check if the user is in the DB
    User.findOne({ username: data.username }, function(err, user) {
      if (err) throw err;
      console.info('User: ' + data.username + ', is attempting to login');
      // If user exists, send user info
      if(user){
        console.info('User: ' + user.username + ' has connected to server');
        username = user.username;
        userLeague = user.league;
        basket[data.username] = socket.id;
        console.info('LOGIN SUCCESS: user --> ' + user.username + ' with socket: ' + basket[data.username]);
        socket.emit('LOGIN_SUCCESS', user);
      } else { // if user does not, log failed attempt
        socket.emit('LOGIN_FAILED');
        console.info('User: ' + data.username + ', has failed to login.');
      }
    });
  });

  socket.on('CREATE_ACCOUNT', function(data) {
    User.findOne({ username: data.username }, function(err, user) {
      if (err) {
        console.info('error thrown when create account');
        //throw err;
      }
      if(user){ // if username already exist, tell the socket.
        console.info('Failed to create user with username: ' + user.username);
        socket.emit('CREATE_ACCOUNT_FAILED');
      } else {  // if username does not exist, create user
        console.info('adding user...');
        var newUser = User({
          username: data.username,
          league: FirstLeague,
        });

        // save the new user to DB
        newUser.save(function(err) {
          if (err) {
            socket.emit('CREATE_ACCOUNT_FAILED');
            console.info('Failed to create user with username: ' + user.username);
          }
          console.log('New user: ' + data.username + ', was created.');
          socket.emit('CREATE_ACCOUNT_SUCCESS');
          username = data.username;
          basket[data.username] = socket.id;
        });
      }
    });
  });

  socket.on('TEST_CONNECTION', function() {
      console.info('connection successful, socket has been opened.');
      socket.emit('TEST_CONNECTION_SUCCESS');
  });

  socket.on('USER_CONNECT', function (data) {
    console.info('User Connected.');

    userLeague = data.league;
    username = data.username;

    socket.emit('USER_CONNECTED', {onlineClientInfoByLeague: "it worked!"});
  });

  socket.on('START_BATTLE', function(data) {
    console.info('user: ' + username + ', attempting to find opponent');
    opponentTeam = [];

    User.find({ league: data.league }, function (err, users) {
      var i = 0;
      while (i < 10) {
        // Get a random entry
        var random = Math.floor(Math.random() * users.length);
        if(users[random].username != username && users[random].team.length >= 9) {
          console.info('opponent found --> ' + users[random].username);
          socket.emit('PRE_BATTLE_INFO', users[random]);
          opponentTeam = users[random].team;
          User.findOne({username: username}, function (err, user) {
            //console.info('userteam: ' + user.team + '\nopponentTeam: ' + opponentTeam);
            user.energy--;
            var results = Battle.GetScore(user.team, opponentTeam);
            var userResults = _.object(['inning1', 'inning2', 'inning3', 'inning4', 'inning5', 'inning6', 'inning7', 'inning8', 'inning9', 'total'], results.user);
            var opponentResults = _.object(['inning1', 'inning2', 'inning3', 'inning4', 'inning5', 'inning6', 'inning7', 'inning8', 'inning9', 'total'], results.opponent);
            if (userResults.total > opponentResults.total) {
              user.wins++
            } else if (userResults.total < opponentResults.total) {
              user.losses++;
            }
            console.info('sending battle results to user: ' + username);
            socket.emit('BATTLE_RESULTS', {'energy': user.energy, 'battleResults': {user: userResults, opponent: opponentResults}, 'wins': user.wins, 'losses': user.losses });
            user.save();
          });
          break;
        }
        i++;
      }
    });
  });

  socket.on('LOOK_FOR_FRIEND',  function (data) {
      if(basket[data.friendUsername]) {
          console.info('sending battle request to friend: ' + data.friendUsername + ' from user: ' + username);
          var to = basket[data.friendUsername];
          io.to(to).emit('FRIEND_BATTLE_REQUEST', {'friendUsername': username});
      } else {
        console.info('friend: ' + data.friendUsername + ' is offline so user: ' + username + ' cannot start battle');
        socket.emit('FRIEND_OFFLINE');
      }
  });

  socket.on('FRIEND_BATTLE_CANCELLED', function (data) {
    console.info('user:' + username + ' cancelled battle');
    var to = basket[data.friendUsername];
    console.info('sending battle cancelation to user: ' + data.friendUsername + ' on socket: ' +  to);
    io.to(to).emit('FRIEND_BATTLE_CANCELLED');
  });

  socket.on('START_FRIEND_BATTLE', function (data) {
    console.info('starting friend battle');

    // -------------- Get friend's information -------------
    console.info('getting friend info: ' + data.friendUsername);
    User.findOne({username: data.friendUsername}, function (err, friend) { // friend is the friend that requested the battle
      if (err) {
        console.info('friend for battle not found.');
      } else if (friend) {
        console.info('friend for battle found --> ' + friend.username);
        console.info('finished gathering friend info');
        User.findOne({username: username}, function (err, user) {
          if (err) {
            console.info('error when searching for user db.');
          }else if (user) {
            // --------- Send Pre Battle info ------------------------
            console.info('sending pre battle info to both clients');
            socket.emit('PRE_BATTLE_INFO', friend); // sending the request friend the original challenger's info
            console.info('sending user: ' + friend.username + ' on socket: ' + basket[friend.username] + ' prebattle info.');
            io.to(basket[friend.username]).emit('PRE_BATTLE_INFO', user); // sending the friend that requested the battle the info of friend

            var results = Battle.GetScore(user.team, friend.team);
            // friend that requested challenges battle results
            var userResults = _.object(InningNames, results.user);
            // requested friend's battle results
            var friendResults = _.object(InningNames, results.opponent);
            // check who won and account for it
            if (userResults.total > friendResults.total) {
              user.wins++
              friend.losses++;
            } else if (userResults.total < friendResults.total) {
              user.losses++;
              friend.wins++;
            }

            user.energy--;
            friend.energy--;

            // ------------ Send Battle Results ---------------------
            console.info('sending battle results to users: ' + username + ' and ' + friend.username);
            socket.emit('BATTLE_RESULTS', {'energy': user.energy, 'battleResults': {user: userResults, opponent: friendResults}, 'wins': user.wins, 'losses': user.losses });
            io.to(basket[friend.username]).emit('BATTLE_RESULTS', {'energy': friend.energy, 'battleResults': {user: friendResults, opponent: userResults}, 'wins': friend.wins, 'losses': friend.losses });
          }
          user.save();
        });
        friend.save();
      }
    });
  });

  socket.on('START_TRAINING', function(data) {
    setTimeout(function() {
      console.info('Training results to user: ' + username + 'are done.');
      //console.info('sending Training results to user: ' + JSON.stringify(data));
      User.findOne({username: data.username}, function (err, user) {
        user.exp += parseInt(data.expGain);
        for (var i = 0; i < user.team.length; i++) {
          user.team[i][data.stat] += parseInt(data.statGain);
        }
        user.energy++;
        console.info('sending training results to user: ' + username);
        socket.emit('TRAINING_RESULTS', user );
        user.save();
      });
    }, data.trainingTimeLength);
  });

  socket.on('ADD_PLAYER', function (data) {
    User.findOne({ username: username }, function(err, user) {
      if (err) {
        console.info('error thrown when adding player.');
        //throw err;
      }
      if(user){ // if username already exist, add player to team.
        user.team.push(data);
        User.findOneAndUpdate({ username: username }, { team: user.team }, function(err, user) {
          if (err) {
            console.info('Failed to add player to team of user: ' + username);
            socket.emit('ADD_PLAYER_FAILED');
          }

          console.info('Added player to team of user: ' + username);
        });
      } else {  // if username does not exist, create user
        console.info('user: ' + username + ', does not exist');
        socket.emit('ADD_PLAYER_FAILED');
        return;
      }
    });
  });

  socket.on('UPDATE_TEAM', function (data) {
    User.findOne({ username: username }, function(err, user) {
      if (err) {
        console.info('error thrown when updating team.');
        //throw err;
      }
      if(user){ // if username already exist, add player to team.
          user.team = data.team;
          console.info('Updated team of user: ' + username);
          socket.emit('UPDATE_TEAM_SUCCESS', { 'team': user.team});
          user.save();
      } else {  // if username does not exist, create user
        console.info('user: ' + username + ', does not exist');
      }
    });
  });

  socket.on('SWAP_PLAYER', function (data) {
    //console.info('SWAP attempting --> ' + JSON.stringify(data));
    User.findOne({ username: username }, function(err, user) {
      if (err) {
        console.info('error thrown when swapping player.');
        //throw err;
      }
      if(user){ // if username already exist, add player to team.
        // get oldPlayer object in array
        var oldPlayer = _.findWhere(user.team, {name: data.oldPlayer.name});
        user.team = _.without(user.team, oldPlayer);
        user.team.push(data.newPlayer);
        User.findOneAndUpdate({ username: username }, { team: user.team }, function(err, user) {
          if (err) {
            console.info('Failed to swap player to team of user: ' + username);
            socket.emit('SWAP_PLAYER_FAILED');
          }
          console.info('Swapped player to team of user: ' + username + '. swap: ' + data.oldPlayer.name + ' --> ' + data.newPlayer.name);
        });
      } else {  // if username does not exist, create user
        console.info('user: ' + username + ', does not exist');
        socket.emit('SWAP_PLAYER_FAILED');
        return;
      }
    });
  });

  socket.on('UPDATE_STATS', function(data) {
    // update DB of stats
    User.findOne({username: username}, function(err, user) {
      user.wins = data.wins;
      user.lossess = data.losses;
      user.exp = data.exp;
      user.energy = data.energy;
      user.league = data.league;

      console.info('saving update stats for user: ' + username);
      user.save();
    });
  });

  socket.on('GET_STATS', function() {
    User.findOne({ username: username }, function(err, user) {
      if (err) {
        console.info('User does not exist, cannot get stats.');
        //throw err;
      }
      console.info('sending stats to user: ' + user.username);
      socket.emit('GET_STATS_SUCCESS', user);
    });
  });

  socket.on('GET_LEADERBOARD_INFO', function (data, callback) {
    User.find({}, 'username exp', function (err, users) {
      //console.info(JSON.stringify(_.sortBy(users, 'exp')));
      console.info('Sending Leaderboard to user: ' + username);
      socket.emit('GET_LEADERBOARD_INFO_SUCCESS', { leaderboard: _.sortBy(users, 'exp') });
    });
  })

  socket.on('disconnect', function () {
    console.info('Socket: ' + socket.id + ', user: ' + username + ' is disconnected');
    delete basket[username];
  });
});

server.listen(port,  '0.0.0.0', function() {
  console.log('Listening on port ' + port);
});
