const mongoose    = require('mongoose');

const Schema = mongoose.Schema;

var playerSchema = new Schema ({
  spriteNum: Number,
  name: String,
  pitching: Number,
  batting: Number,
  fielding: Number,
  running: Number,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

var userSchema = new Schema({
  username: { type: String, required: true, unique: true },
  socketId: { type: String, default: '0'},
  league: { type: String, default: 'Pee Wee League' },
  team: [playerSchema],
  exp:  { type: Number, default: 0 },
  energy:  { type: Number, default: 5 },
  wins:  { type: Number, default: 0 },
  losses: { type: Number, default: 0 },
  isTraining: {type: Boolean, default: false },
  isOnline: {type: Boolean, default: false},
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

var User = mongoose.model('User', userSchema);
var Player = mongoose.model('Player', playerSchema);

module.exports= {
  User: User,
  Player: Player
}
