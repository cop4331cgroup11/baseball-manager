var Bases={
  bases:0,
  Push: function(bit)
  {
  	//this push is different the the push I originally thought off.
  	//it should work the same, but you will not have the bitwise operators.
  	// -Joe
      this.bases = this.bases*10;
      this.bases = this.bases+bit;
      return this.MSB();
  },
  MSB: function()
  {

      var tBases = this.bases;
      //reduces number down to one digit (Equivalant to >>3)
  	tBases = tBases / 1000;
  	//probably not necessary
      tBases = Math.floor(tBases);

  	//should reset the first digit to zero.
      while(this.bases >= 1000)		//changed from 1111 to 1000. We do not care what first-third has on base
      {
          this.bases = this.bases - 1000;	//changed from 10000 to 1000. subtracting 10000 would just make it negative.
  										//should also only be 4 digits long at the most.
      }
      return tBases;
  },
  resetBases: function()
  {
      this.bases =0;
  }
};

var Score={
  team1: 0,
  team2: 0,
  turn: false,
  Increase: function(n)
  {
      if(this.turn)
      {
          this.team2+=n;
      }
      else
      {
          this.team1+=n;
      }
  },
  Swap: function()
  {
      this.turn = !this.turn;
  }
};

function Game(A, B) {
  var Results = {
    user: [],
    opponent: []
  };

  Bases.bases = 0;
  Score.team1 = 0;
  Score.team2 = 0;
  Score.turn = false;

	for(var i = 0; i < 9; i++)
	{
    Score.team1 = 0;
    Score.team2 = 0;
		Inning(A, B);
    Results.user.push(Score.team1);
    Results.opponent.push(Score.team2);
	}
  Results.user.push( Results.user.reduce(add, 0) );
  Results.opponent.push( Results.opponent.reduce(add, 0) );
	return Results;
}

function Inning(team1,team2)
{
    var outCounter = 0;
    var i = 0;

    while(outCounter < 3)
    {
        if (Score.team1 > 99) {
          break; // PLEASE BREAK
        }
        if(BaseCalc(team1[i],team2) == 1)
        {
            outCounter++;
        }
        i++;
        if(i==9)
        {
            i=0;
        }
    }

    Score.Swap();
    outCounter = 0;
    i=0;
        while(outCounter < 3)
    {
      if (Score.team2 > 99) {
        break; // PLEASE BREAK
      }
      if(BaseCalc(team2[i],team1) == 1)
      {
          outCounter++;
      }
      i++;
      if(i==9)
      {
          i=0;
      }
    }
}

function BaseCalc(offense, defense)
{
    tempPitcher = defense[0];
    if(Bat(offense,tempPitcher))
    {
        var tmpN=MoveBase(CompareStats(offense.running, defense[(Math.floor(Math.random()*100) % 9)].fielding));
        if(tmpN === 0)
        {
            return 1;
        }
        Score.Increase(tmpN);
        return 0;
    }
}

function Bat(batter, pitcher)
{
    for(var i = 0; i<3; i++)
    {
        if(CompareStats(batter.batting,pitcher.pitching) > 0)
        {
            return true;
        }
    }
    return false;
}

function CompareStats(playerA,playerB)
{
    var rng = Math.random();
    rng = (rng * 100) % 100;
    rng = Math.floor(rng);
    var tA = playerA - rng;
	rng = Math.random();
    rng = (rng * 100) % 100;
    rng = Math.floor(rng);
    var tB = playerB - rng;

    if(tA < tB)
    {
        return 0;
    }
    if(tA-tB < 10)
    {
        return 1;
    }
    if(tA-tB < 25)
    {
        return 2;
    }
    if(tA-tB < 50)
    {
        return 3;
    }
    return 4;
}

function MoveBase(n)
{
    if(n==0)
    {
        return 0;
    }
    var score = 0;
    score += Bases.Push(1);
    n--;
    for(; n>0;n--)
    {
        score+=Bases.Push(0);
    }
    return score;
}

function add (a, b) {
  return a + b;
}

module.exports = {
  GetScore: Game
};

var testTeam1 = [
        {
            "name": "BOSHKEVAL",
            "spriteNum": 31,
            "batting": 99,
            "fielding": 78,
            "pitching": 41,
            "running": 9,
            "_id": {
                "$oid": "58366fa52d5a6817dcf61f50"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:13.405Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:13.405Z"
            }
        },
        {
            "name": "OTABAMDON",
            "spriteNum": 22,
            "batting": 103,
            "fielding": 24,
            "pitching": 89,
            "running": 6,
            "_id": {
                "$oid": "58366fa62d5a6817dcf61f51"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:14.434Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:14.434Z"
            }
        },
        {
            "name": "BAMKEVCOD",
            "spriteNum": 7,
            "batting": 117,
            "fielding": 50,
            "pitching": 69,
            "running": 25,
            "_id": {
                "$oid": "58366fa62d5a6817dcf61f52"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:14.943Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:14.943Z"
            }
        },
        {
            "name": "BOSHSHILAL",
            "spriteNum": 16,
            "batting": 73,
            "fielding": 88,
            "pitching": 17,
            "running": 33,
            "_id": {
                "$oid": "58366fa72d5a6817dcf61f53"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:15.660Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:15.660Z"
            }
        },
        {
            "name": "PEPEOTABOSH",
            "spriteNum": 18,
            "batting": 111,
            "fielding": 23,
            "pitching": 8,
            "running": 90,
            "_id": {
                "$oid": "58366fa82d5a6817dcf61f54"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:16.207Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:16.207Z"
            }
        },
        {
            "name": "JOOTACON",
            "spriteNum": 9,
            "batting": 60,
            "fielding": 36,
            "pitching": 73,
            "running": 45,
            "_id": {
                "$oid": "58366faa2d5a6817dcf61f55"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:18.935Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:18.935Z"
            }
        },
        {
            "name": "OLODONKEV",
            "spriteNum": 2,
            "batting": 63,
            "fielding": 94,
            "pitching": 26,
            "running": 20,
            "_id": {
                "$oid": "58366fab2d5a6817dcf61f56"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:19.550Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:19.550Z"
            }
        },
        {
            "name": "CONMARATA",
            "spriteNum": 6,
            "batting": 26,
            "fielding": 43,
            "pitching": 91,
            "running": 82,
            "_id": {
                "$oid": "58366fac2d5a6817dcf61f57"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:20.267Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:20.267Z"
            }
        },
        {
            "name": "ATABOSHSHIL",
            "spriteNum": 19,
            "batting": 64,
            "fielding": 77,
            "pitching": 83,
            "running": 40,
            "_id": {
                "$oid": "58366fad2d5a6817dcf61f58"
            },
            "updated_at": {
                "$date": "2016-11-24T04:42:21.033Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:42:21.033Z"
            }
        }
    ];

    var testTeam2 = [
        {
            "name": "CONDONSHIL",
            "spriteNum": 8,
            "batting": 35,
            "fielding": 16,
            "pitching": 49,
            "running": 39,
            "_id": {
                "$oid": "5836707f2d5a6817dcf61f6e"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:51.618Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:51.618Z"
            }
        },
        {
            "name": "DYLMARCOD",
            "spriteNum": 28,
            "batting": 52,
            "fielding": 6,
            "pitching": 20,
            "running": 35,
            "_id": {
                "$oid": "5836707f2d5a6817dcf61f6f"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:51.824Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:51.824Z"
            }
        },
        {
            "name": "ALCODKEV",
            "spriteNum": 24,
            "batting": 69,
            "fielding": 10,
            "pitching": 74,
            "running": 86,
            "_id": {
                "$oid": "5836707f2d5a6817dcf61f70"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:51.942Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:51.942Z"
            }
        },
        {
            "name": "MARKEVDON",
            "spriteNum": 18,
            "batting": 34,
            "fielding": 30,
            "pitching": 53,
            "running": 16,
            "_id": {
                "$oid": "583670802d5a6817dcf61f71"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:52.232Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:52.232Z"
            }
        },
        {
            "name": "DYLPEPEOLO",
            "spriteNum": 7,
            "batting": 2,
            "fielding": 12,
            "pitching": 22,
            "running": 23,
            "_id": {
                "$oid": "583670802d5a6817dcf61f72"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:52.296Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:52.296Z"
            }
        },
        {
            "name": "ALATABOSH",
            "spriteNum": 3,
            "batting": 67,
            "fielding": 32,
            "pitching": 99,
            "running": 53,
            "_id": {
                "$oid": "583670802d5a6817dcf61f73"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:52.440Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:52.440Z"
            }
        },
        {
            "name": "JOPEPESHIL",
            "spriteNum": 24,
            "batting": 93,
            "fielding": 9,
            "pitching": 65,
            "running": 36,
            "_id": {
                "$oid": "583670802d5a6817dcf61f74"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:52.568Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:52.568Z"
            }
        },
        {
            "name": "ATADYLMAR",
            "spriteNum": 29,
            "batting": 99,
            "fielding": 41,
            "pitching": 10,
            "running": 56,
            "_id": {
                "$oid": "583670802d5a6817dcf61f76"
            },
            "updated_at": {
                "$date": "2016-11-24T04:45:52.864Z"
            },
            "created_at": {
                "$date": "2016-11-24T04:45:52.864Z"
            }
        },
        {
            "name": "OTAMARPEPE",
            "spriteNum": 4,
            "batting": 67,
            "fielding": 39,
            "pitching": 87,
            "running": 12,
            "_id": {
                "$oid": "58367e8a6f778023f0b58b36"
            },
            "updated_at": {
                "$date": "2016-11-24T05:45:46.252Z"
            },
            "created_at": {
                "$date": "2016-11-24T05:45:46.252Z"
            }
        }
    ];
