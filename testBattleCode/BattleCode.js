var Bases={
  bases:0,
  Push: function(bit)
  {
  	//this push is different the the push I originally thought off.
  	//it should work the same, but you will not have the bitwise operators.
  	// -Joe
      this.bases = this.bases*10;
      this.bases = this.bases+bit;
      return this.MSB();
  },
  MSB: function()
  {

      var tBases = this.bases;
      //reduces number down to one digit (Equivalant to >>3)
  	tBases = tBases / 1000;
  	//probably not necessary
      tBases = Math.floor(tBases);

  	//should reset the first digit to zero.
      while(this.bases > 1000)		//changed from 1111 to 1000. We do not care what first-third has on base
      {
          this.bases = this.bases - 1000;	//changed from 10000 to 1000. subtracting 10000 would just make it negative.
  										//should also only be 4 digits long at the most.
      }
      return tBases;
  },
  resetBases: function()
  {
      this.bases =0;
  }
};

var Score={
  team1: 0,
  team2: 0,
  turn: false,
  Increase: function(n)
  {
      if(this.turn)
      {
          this.team2+=n;
      }
      else
      {
          this.team1+=n;
      }
  },
  Swap: function()
  {
      this.turn = !this.turn;
  }
};

function Game(A, B) {
  var Results = {
    user: [],
    opponent: []
  };

  Bases.bases = 0;
  Score.team1 = 0;
  Score.team2 = 0;
  Score.turn = false;

	for(var i = 0; i < 9; i++)
	{
    Score.team1 = 0;
    Score.team2 = 0;
		Inning(A, B);
    Results.user.push(Score.team1);
    Results.opponent.push(Score.team2);
	}
  Results.user.push( Results.user.reduce(add, 0) );
  Results.opponent.push( Results.opponent.reduce(add, 0) );
	return Results;
}

function Inning(team1,team2)
{
    var outCounter = 0;
    var i = 0;

    while(outCounter < 3 && Score.team1 < 10)
    {
      console.info(' whileloop 1 team1score: ' + Score.team1);
      if(BaseCalc(team1[i],team2) == 1)
      {
          outCounter++;
      }
      i++;
      if(i==9)
      {
          i=0;
      }
    }

    Score.Swap();
    outCounter = 0;
    i=0;

    while(outCounter < 3 && Score.team2 < 10)
    {
      console.info('whileloop 2 team2Score: ' + Score.team2);
      if(BaseCalc(team2[i],team1) == 1)
      {
          outCounter++;
      }
      i++;
      if(i==9)
      {
          i=0;
      }
    }
}

function BaseCalc(offense, defense)
{
    tempPitcher = defense[0];
    if(Bat(offense,tempPitcher))
    {
        var tmpN=MoveBase(CompareStats(offense.running, defense[(Math.floor(Math.random()*100) % 9)].fielding));
        if(tmpN === 0)
        {
            return 1;
        }
        Score.Increase(tmpN);
        return 0;
    }
}

function Bat(batter, pitcher)
{
    for(var i = 0; i<3; i++)
    {
        if(CompareStats(batter.batting,pitcher.pitching) > 0)
        {
            return true;
        }
    }
    return false;
}

function CompareStats(playerA,playerB)
{
    var rng = Math.random();
    rng = (rng * 100) % 100;
    rng = Math.floor(rng);
    var tA = playerA - rng;
	rng = Math.random();
    rng = (rng * 100) % 100;
    rng = Math.floor(rng);
    var tB = playerB - rng;

    if(tA < tB)
    {
        return 0;
    }
    if(tA-tB < 10)
    {
        return 1;
    }
    if(tA-tB < 25)
    {
        return 2;
    }
    if(tA-tB < 50)
    {
        return 3;
    }
    return 4;
}

function MoveBase(n)
{
    if(n==0)
    {
        return 0;
    }
    var score = 0;
    score += Bases.Push(1);
    n--;
    for(; n > 0;n--)
    {
        score+=Bases.Push(0);
    }
    return score;
}

function add (a, b) {
  return a + b;
}

// module.exports = {
//   GetScore: Game
// };

var testTeam1 = [
        {
            "name": "ALBAMATA",
            "spriteNum": 7,
            "batting": 490,
            "fielding": 381,
            "pitching": 371,
            "running": 431,
            "_id": {
                "$oid": "5840e162da188d4f482ea3ec"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.512Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.512Z"
            }
        },
        {
            "name": "BOSHCONPEPE",
            "spriteNum": 29,
            "batting": 467,
            "fielding": 426,
            "pitching": 350,
            "running": 406,
            "_id": {
                "$oid": "5840e162da188d4f482ea3eb"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.511Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.511Z"
            }
        },
        {
            "name": "BAMCONAL",
            "spriteNum": 21,
            "batting": 486,
            "fielding": 419,
            "pitching": 325,
            "running": 438,
            "_id": {
                "$oid": "5840e162da188d4f482ea3ea"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.511Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.511Z"
            }
        },
        {
            "name": "MARCONJO",
            "spriteNum": 17,
            "batting": 444,
            "fielding": 431,
            "pitching": 364,
            "running": 393,
            "_id": {
                "$oid": "5840e162da188d4f482ea3e9"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.510Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.510Z"
            }
        },
        {
            "name": "OTASHILBAM",
            "spriteNum": 25,
            "batting": 480,
            "fielding": 437,
            "pitching": 293,
            "running": 429,
            "_id": {
                "$oid": "5840e162da188d4f482ea3e8"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.510Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.510Z"
            }
        },
        {
            "name": "OTAALBAM",
            "spriteNum": 10,
            "batting": 472,
            "fielding": 393,
            "pitching": 345,
            "running": 420,
            "_id": {
                "$oid": "5840e162da188d4f482ea3e7"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.510Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.510Z"
            }
        },
        {
            "name": "BAMATAMAR",
            "spriteNum": 26,
            "batting": 479,
            "fielding": 398,
            "pitching": 301,
            "running": 411,
            "_id": {
                "$oid": "5840e162da188d4f482ea3e6"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.509Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.509Z"
            }
        },
        {
            "name": "JOCODCOD",
            "spriteNum": 19,
            "batting": 424,
            "fielding": 445,
            "pitching": 368,
            "running": 469,
            "_id": {
                "$oid": "5840e162da188d4f482ea3e5"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.509Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.509Z"
            }
        },
        {
            "name": "KEVJOOTA",
            "spriteNum": 18,
            "batting": 452,
            "fielding": 430,
            "pitching": 283,
            "running": 477,
            "_id": {
                "$oid": "5840e162da188d4f482ea3e4"
            },
            "updated_at": {
                "$date": "2016-12-02T02:50:10.506Z"
            },
            "created_at": {
                "$date": "2016-12-02T02:50:10.506Z"
            }
        }
    ];

    var testTeam2 = [
        {
            "name": "BAMOTAOLO",
            "spriteNum": 10,
            "batting": 95,
            "fielding": 26,
            "pitching": 54,
            "running": 89,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546ef"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.299Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.299Z"
            }
        },
        {
            "name": "OLODONBOSH",
            "spriteNum": 8,
            "batting": 101,
            "fielding": 71,
            "pitching": 88,
            "running": 90,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546ee"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.299Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.299Z"
            }
        },
        {
            "name": "JOJOAL",
            "spriteNum": 27,
            "batting": 88,
            "fielding": 95,
            "pitching": 43,
            "running": 90,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546ed"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.299Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.299Z"
            }
        },
        {
            "name": "DONPEPECON",
            "spriteNum": 19,
            "batting": 98,
            "fielding": 50,
            "pitching": 100,
            "running": 86,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546ec"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.298Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.298Z"
            }
        },
        {
            "name": "DYLOLOKEV",
            "spriteNum": 3,
            "batting": 103,
            "fielding": 62,
            "pitching": 89,
            "running": 53,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546eb"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.298Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.298Z"
            }
        },
        {
            "name": "DONALKEV",
            "spriteNum": 20,
            "batting": 68,
            "fielding": 81,
            "pitching": 92,
            "running": 75,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546ea"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.298Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.298Z"
            }
        },
        {
            "name": "PEPEPEPEAL",
            "spriteNum": 3,
            "batting": 91,
            "fielding": 93,
            "pitching": 64,
            "running": 84,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546e9"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.297Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.297Z"
            }
        },
        {
            "name": "BOSHPEPEPEPE",
            "spriteNum": 18,
            "batting": 111,
            "fielding": 98,
            "pitching": 67,
            "running": 101,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546e8"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.297Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.297Z"
            }
        },
        {
            "name": "ALOTABOSH",
            "spriteNum": 29,
            "batting": 90,
            "fielding": 95,
            "pitching": 37,
            "running": 70,
            "_id": {
                "$oid": "583ccdd99709c51ebeb546e7"
            },
            "updated_at": {
                "$date": "2016-11-29T00:37:45.297Z"
            },
            "created_at": {
                "$date": "2016-11-29T00:37:45.297Z"
            }
        }
    ];
