﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using SocketIO;
using UnityEngine.UI;

public class Login : MonoBehaviour
{

    private SocketIOComponent socket;
    private User m_scriptUser;
    private LevelManager m_scriptLevelManager;

    void Awake()
    {
        socket = GameObject.FindObjectOfType<SocketIOComponent>();
        m_scriptUser = GameObject.FindObjectOfType<User>();
        m_scriptLevelManager = GameObject.FindObjectOfType<LevelManager>();
    }

    void Start()
    {
        socket.On("LOGIN_SUCCESS", LoginSuccessful);
        socket.On("LOGIN_FAILED", LoginFailed);
    }

    public void AttemptLogin()
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["username"] = GameObject.FindObjectOfType<InputField>().text;  

        socket.Emit("ATTEMPT_LOGIN", new JSONObject(data));
        GameObject.FindObjectOfType<InputField>().text = "...";
    }

    public void LoginSuccessful(SocketIOEvent e)
    {
        Debug.Log(e.data);
        m_scriptUser.Login(e.data);

        GameObject.FindObjectOfType<InputField>().text = "success...";

        if (m_scriptUser.ObjTeam.ListRoster.Count < 9)
        {
            m_scriptLevelManager.LoadLevel("04a Player Selection");
        }
        else
        {
            m_scriptLevelManager.LoadLevel("02a Main Screen");
        }
    }

    public void LoginFailed(SocketIOEvent e)
    {
        Debug.Log("Login Failed");
        GameObject.FindObjectOfType<InputField>().text = "failed...";
    }

    // Update is called once per frame
    void Update()
    {

    }
}
