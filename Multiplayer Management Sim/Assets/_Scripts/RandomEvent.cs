﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SocketIO;

public class RandomEvent : MonoBehaviour {

     private Team m_objTeam;
     private LevelManager m_objLevelManager;
     private List<string> m_strRandomEvents;
     private GameObject[] m_objCards;
     private SocketIOComponent socket;

    #region Properties
    public List<string> strRandomEvents
     {
          get { return m_strRandomEvents; }
          set { m_strRandomEvents = value; }
     }
     #endregion

     // Use this for initialization
     void Start () {
          socket = GameObject.FindObjectOfType<SocketIOComponent>();
          m_objTeam = GameObject.FindObjectOfType<User>().ObjTeam;
          m_objLevelManager = GameObject.FindObjectOfType<LevelManager>();
          m_strRandomEvents = new List<string>();
          PopulateRandomEventList();

          if (SceneManager.GetActiveScene().name == "04a Player Selection")
          {
               m_objCards = GameObject.FindGameObjectsWithTag("BigCardTemplate");
               PopulatePlayerCards();

               if(m_objTeam.ListRoster.Count < 9)
               {
                    GameObject.Find("Select None Wrapper").transform.GetChild(0).gameObject.SetActive(false);
                    GameObject.FindGameObjectWithTag("RandomEvent").GetComponent<Text>().text = "Draft Day: Select a player to add to your team";
               }
               else
               {
                    SpawnRandomEventText();
                    GameObject.Find("Select None Wrapper").transform.GetChild(0).gameObject.SetActive(true);
               }
          }



     }
	
     /// <summary>
     /// Fills the list of random event with funny strings explaining the situation for gaining a new player
     /// </summary>
     void PopulateRandomEventList()
     {
          m_strRandomEvents.Add("On his vacation to Cuba, your cousin Paco found 3 potential superstar players..." +
               " Though you only have enough money to smuggle one of them into the country. Pick one.");

          m_strRandomEvents.Add("After rolling in toxic waste for several hours, 3 potential players develop super powers. " +
               "Though on the downside, they also develop extreme cases of radiation poisoning. You only have enough money to pay " +
               "for the hospital bills of one of them. Pick one.");

          m_strRandomEvents.Add("While sitting on a toilet, you are abducted by a group of 3 space aliens who have heard about your" +
               " team. They all want to play for you, but unfortunately they all hate each other and refuse to play together. " +
               "Choose the one you want to recruit.");
     }

     /// <summary>
     /// Triggers a random event
     /// </summary>
     public void TriggerRandomEvent()
     {
          m_objLevelManager.LoadLevel("04a Player Selection");
     }

     /// <summary>
     /// Populates the stats on the player card UI
     /// </summary>
     void PopulatePlayerCards()
     {
          for(int i = 0; i < 3; i++)
          {
               m_objCards[i].gameObject.transform.GetChild(0).GetComponent<Text>().text = m_objCards[i].GetComponent<PlayerCard>().StrPlayerName;                                  // get player name
               m_objCards[i].gameObject.transform.GetChild(2).GetComponent<Image>().sprite = m_objCards[i].GetComponent<PlayerCard>().ObjPlayerSprite;                             // get player sprite
               m_objCards[i].gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = m_objCards[i].GetComponent<PlayerCard>().IntBatting.ToString();    // get player batting
               m_objCards[i].gameObject.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = m_objCards[i].GetComponent<PlayerCard>().IntFielding.ToString();   // get player fielding
               m_objCards[i].gameObject.transform.GetChild(5).transform.GetChild(0).GetComponent<Text>().text = m_objCards[i].GetComponent<PlayerCard>().IntPitching.ToString();   // get player pitching
               m_objCards[i].gameObject.transform.GetChild(6).transform.GetChild(0).GetComponent<Text>().text = m_objCards[i].GetComponent<PlayerCard>().IntRunning.ToString();    // get player running

          }
     }


     /// <summary>
     /// Spawns the text on the random event scene
     /// </summary>
     void SpawnRandomEventText()
     {
          int intIndex = Random.Range(0, m_strRandomEvents.Count);
          Text objText = GameObject.FindGameObjectWithTag("RandomEvent").GetComponent<Text>();

          objText.text= m_strRandomEvents[intIndex];
     }


     /// <summary>
     /// Adds the player to the team
     /// </summary>
     public void AddPlayer()
     {
          if(m_objTeam.ListRoster.Count < 9)
          {
               m_objTeam.AddPlayer(gameObject.transform.parent.GetComponent<PlayerCard>().Player);

               Dictionary<string, string> data = gameObject.transform.parent.GetComponent<PlayerCard>().Player.ToDictionary();
               Debug.Log(new JSONObject(data));
               socket.Emit("ADD_PLAYER", new JSONObject(data));

               if(m_objTeam.ListRoster.Count < 9)
               {
                    TriggerRandomEvent();
               }
               else
               {
                    m_objLevelManager.LoadLevel("02a Main Screen");
               }
          }
          else
          {
               m_objTeam.TempPlayer = (gameObject.transform.parent.GetComponent<PlayerCard>().Player);
               m_objLevelManager.LoadLevel("04a Player Replacement");
          }
     }

    public void SwapPlayer()
    {
        Debug.Log("Swapping players...");
        m_objTeam.SwapPlayers(m_objTeam.ListRoster[0], gameObject.transform.parent.GetComponent<PlayerCard>().Player);

        Dictionary<string, JSONObject> data = new Dictionary<string, JSONObject>();
        data["oldPlayer"] = new JSONObject(m_objTeam.ListRoster[0].ToDictionary());
        data["newPlayer"] = new JSONObject(gameObject.transform.parent.GetComponent<PlayerCard>().Player.ToDictionary());

        Debug.Log(new JSONObject(data));
        socket.Emit("SWAP_PLAYER", new JSONObject(data));
        Debug.Log("Swapping completed.");
    }
}
