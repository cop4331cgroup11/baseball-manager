﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team : MonoBehaviour
{

     private List<Player> m_listRoster;
     private Player m_objTempPlayer;
     private Player m_objSelectedPlayer;
     private bool m_blnIsTraining;
     
     #region properties
     public bool BlnIsTraining
     {
          get { return m_blnIsTraining; }
          set { m_blnIsTraining = value; }
     }

     public List<Player> ListRoster
     {
          get { return m_listRoster; }
          set { m_listRoster = value; }
     }

     public Player TempPlayer
     {
          get { return m_objTempPlayer; }
          set { m_objTempPlayer = value; }
     }

     public Player SelectedPlayer
     {
          get { return m_objSelectedPlayer; }
          set { m_objSelectedPlayer = value; }
     }
     #endregion

     void Awake()
     {
          DontDestroyOnLoad(gameObject);
     }

     // Use this for initialization
     void Start ()
    {
        m_blnIsTraining = false;
        m_listRoster = new List<Player>();
	}

     // Update is called once per frame
     void Update ()
     {
		
	}

     /// <summary>
     /// Adds a new player to the team
     /// </summary>
     /// <param name="newPlayer"></param>
     public void AddPlayer(Player objNewPlayer)
     {
        if(m_listRoster.Count < 9)
            m_listRoster.Add(objNewPlayer);
        //else call swap method
     }
   
     /// <summary>
     /// takes two players, and swaps their places within the team. If not on team, return's null
     /// </summary>
     /// <param name="newPlayer"></param>
     public void SwapPlayers(Player objOldPlayer, Player objNewPlayer)
     {
        this.ReplacePlayers(objOldPlayer, objNewPlayer);
        this.ReplacePlayers(objNewPlayer, objOldPlayer);
        
     }
    /// <summary>
    /// takes a player, and replaces with another player.
    /// </summary>
    /// <param name="objOldPlayer"></param>
    /// <param name="objNewPlayer"></param>
    public void ReplacePlayers(Player objOldPlayer, Player objNewPlayer)
    {
        int n = m_listRoster.LastIndexOf(objOldPlayer);
        m_listRoster.Remove(objOldPlayer);
        m_listRoster.Insert(n, objNewPlayer);
    }


}
