﻿using System.Collections;
using System.Collections.Generic;
using SocketIO;
using UnityEngine;

public class User : MonoBehaviour {

     private static User objInstance = null;
     public SocketIOComponent socket;
     private Sprite m_objLogo;
     private Team m_objTeam;
     private Battle m_objBattle;
     private Trainer m_objTrainer;
     private Schedule m_objSchedule;
     private string m_strUsername = "no name";
     private string m_strLeague = "no league";
     private int m_intLogoNumber;
     private int m_intExp = 0;
     private int m_intEnergy = 0;
     private int m_intMaxEnergy = 5;
     private int m_intWins = 0;
     private int m_intLosses = 0;
     private bool m_blnIsBeingChallenged = false;
     private RandomManager m_objRandomManager;
     private System.Random rnd;
     private int m_intWinsBefore;
     private int m_intWinsAfter;

     #region Properties
     public int IntWinsBefore
     {
          get { return m_intWinsBefore; }
          set { m_intWinsBefore = value; }
     }

     public int IntWinsAfter
     {
          get { return m_intWinsAfter; }
          set { m_intWinsAfter = value; }
     }

     public Schedule Schedule
     {
          get { return m_objSchedule; }
          set { m_objSchedule = value; }
     }

     public int intLogoNumber
     {
          get { return m_intLogoNumber; }
          set { m_intLogoNumber = value; }
     }

     public int intExp
     {
          get { return m_intExp; }
          set { m_intExp = value; }
     }

     public int intEnergy
     {
          get { return m_intEnergy; }
          set { m_intEnergy = value; }
     }

     public int intMaxEnergy
     {
          get { return m_intMaxEnergy; }
          set { m_intMaxEnergy = value; }
     }

     public int intWins
     {
          get { return m_intWins; }
          set { m_intWins = value; }
     }

     public int intLosses
     {
          get { return m_intLosses; }
          set { m_intLosses = value; }
     }

     public bool blnIsBeingChallenged
     {
          get { return m_blnIsBeingChallenged; }
          set { m_blnIsBeingChallenged = value; }
     }

    public string StrUsername
    {
        get { return m_strUsername; }
        set { m_strUsername = value; }
    }

     public Team ObjTeam
     {
          get { return m_objTeam; }
          set { m_objTeam = value; }
     }

    public string StrLeague
    {
        get
        {
            return m_strLeague;
        }

        set
        {
            m_strLeague = value;
        }
    }

    public Battle ObjBattle
    {
        get
        {
            return m_objBattle;
        }

        set
        {
            m_objBattle = value;
        }
    }

    #endregion

    // Use this for initialization
    void Start () {
        //m_objRandomManager = GameObject.FindObjectOfType<RandomManager>();
        //rnd = m_objRandomManager.objRandom;
          m_objTeam = GameObject.FindObjectOfType<Team>();
          m_objBattle = new Battle();
          m_objSchedule = new Schedule();
          StartCoroutine(ConnectToServer());

          socket.On("TEST_CONNECTION_SUCCESS", TestConnectSuccess);
          socket.On("TRAINING_RESULTS", TrainComplete);
          socket.On("UPDATE_TEAM_SUCCESS", UpdateTeam);
    }

    IEnumerator ConnectToServer()
    {
        yield return new WaitForSeconds(0.1f);

        socket.Emit("TEST_CONNECTION");
    }

    // Update is called once per frame
    void Awake () {
          if (objInstance != null && objInstance != this)
          {
               Destroy(gameObject);
          }
          else
          {
               objInstance = this;
               DontDestroyOnLoad(gameObject);
          }
    }

    public void TestConnectSuccess(SocketIOEvent e)
    {
        Debug.Log("Socket connected and opened.");
    }

    /// <summary>
    /// Contacts the server and logs into the given account
    /// </summary   
    public void Login(JSONObject json)
    {
        Dictionary<string, string> dict = json.ToDictionary();
        this.StrUsername = dict["username"];
        this.StrLeague = dict["league"];
        this.m_intExp = int.Parse(dict["exp"]);
        this.m_intEnergy = int.Parse(dict["energy"]);
        if(m_intEnergy > m_intMaxEnergy)
        {
            m_intEnergy = m_intMaxEnergy;
        }
        this.m_intWins = int.Parse(dict["wins"]);
        this.m_intLosses = int.Parse(dict["losses"]);

        List<Player> roster = new List<Player>();
        for(int i = 0; i < json["team"].Count; i++)
        {
            Dictionary<string, string> tempDict = json["team"][i].ToDictionary();
            //Debug.Log(tempDict["name"]);
            Player tempPlayer = new Player(
                tempDict["name"],
                int.Parse(tempDict["spriteNum"]),
                int.Parse(tempDict["batting"]),
                int.Parse(tempDict["fielding"]),
                int.Parse(tempDict["pitching"]),
                int.Parse(tempDict["running"])
            );
            roster.Add(tempPlayer);
        }

        m_objTeam.ListRoster = roster;
        Debug.Log("Finished adding user stats to User Object.");
    }

    public void SetOpponent(JSONObject json)
    {
        m_objBattle.StrOpponentUsername = json.ToDictionary()["username"];
        Debug.Log("Added opponent's username to Battle object.");

        List<Player> opponentRoster = new List<Player>();
        for (int i = 0; i < json["team"].Count; i++)
        {
            Dictionary<string, string> tempDict = json["team"][i].ToDictionary();
            Player tempPlayer = new Player(
                tempDict["name"],
                int.Parse(tempDict["spriteNum"]),
                int.Parse(tempDict["batting"]),
                int.Parse(tempDict["fielding"]),
                int.Parse(tempDict["pitching"]),
                int.Parse(tempDict["running"])
            );
            opponentRoster.Add(tempPlayer);
        }
        m_objBattle.ListOpponentRoster = opponentRoster;
        Debug.Log("Added opponent's team to Battle object.");
    }

    /// <summary>
    /// Creates a new account on the server
    /// </summary>
    public void CreateAccount()
    {
    }

    public void TrainComplete(SocketIOEvent e)
    {
        Debug.Log("Training completed, giving players updated stats");
        JSONObject json = e.data;
        Dictionary<string, string> dict = json.ToDictionary();

        this.intExp = int.Parse(dict["exp"]);
        this.intEnergy = int.Parse(dict["energy"]);

        List<Player> roster = new List<Player>();

        for (int i = 0; i < json["team"].Count; i++)
        {
            Dictionary<string, string> tempDict = json["team"][i].ToDictionary();

            Player tempPlayer = new Player(
                tempDict["name"],
                int.Parse(tempDict["spriteNum"]),
                int.Parse(tempDict["batting"]),
                int.Parse(tempDict["fielding"]),
                int.Parse(tempDict["pitching"]),
                int.Parse(tempDict["running"])
            );
            roster.Add(tempPlayer);
        }

        this.m_objTeam.ListRoster = roster;

        this.ObjTeam.BlnIsTraining = false;
        Debug.Log("Training complete.");
    }

    public void UpdateTeam(SocketIOEvent e)
    {
        Debug.Log("Updating client of new team");
        var json = e.data;
        List<Player> roster = new List<Player>();
        for (int i = 0; i < json["team"].Count; i++)
        {
            Dictionary<string, string> tempDict = json["team"][i].ToDictionary();
            Player tempPlayer = new Player(
                tempDict["name"],
                int.Parse(tempDict["spriteNum"]),
                int.Parse(tempDict["batting"]),
                int.Parse(tempDict["fielding"]),
                int.Parse(tempDict["pitching"]),
                int.Parse(tempDict["running"])
            );
            roster.Add(tempPlayer);
        }

        m_objTeam.ListRoster = roster;
        Debug.Log("Finished Updating Team.");
    }

     public void UpdateLeague ()
     {
          if (intExp < 100) { m_strLeague = "Pee Wee League"; }
          else if (intExp < 1000) { m_strLeague = "Little League"; }
          else if (intExp < 10000) { m_strLeague = "Junior League"; }
          else if (intExp < 100000) { m_strLeague = "Varsity League"; }
          else if (intExp < 1000000) { m_strLeague = "Minor League"; }
          else { StrLeague = "Major League"; }


     }
}