﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle {

     private List<Player> m_listOpponentRoster;
     private Dictionary<string, int> m_dictUserResults;
    private Dictionary<string, int> m_dictOpponentResults;
     private string m_strOpponentUsername = "No Opponent";
     private bool m_blnIsInBattle;

     #region properties
     public bool BlnIsInBattle
     {
          get { return m_blnIsInBattle; }
          set { m_blnIsInBattle = value; }
     }

    public string StrOpponentUsername
    {
        get { return m_strOpponentUsername; }
        set { m_strOpponentUsername = value; }
    }

    public List<Player> ListOpponentRoster
    {
        get { return m_listOpponentRoster; }
        set { m_listOpponentRoster = value; }
    }

    public Dictionary<string, int> DictOpponentResults
    {
        get { return m_dictOpponentResults; }
        set { m_dictOpponentResults = value; }
    }

    public Dictionary<string, int> DictUserResults
    {
        get { return m_dictUserResults; }
        set { m_dictUserResults = value; }
    }
    #endregion

    // Use this for initialization
    void Start () {
          m_listOpponentRoster = new List<Player>();
          DictUserResults = new Dictionary<string, int>();
          DictOpponentResults = new Dictionary<string, int>();
          m_blnIsInBattle = false;

	}
	
     /// <summary>
     /// Starts a battle with a random user
     /// </summary>
     public void RandomBattle()
     {

     }

     /// <summary>
     /// Starts a battle with the specified user
     /// </summary>
     /// <param name="strUsername"></param>
     public void ChallengeFriend(string strUsername)
     {

     }

     /// <summary>
     /// Prints out results of the battle
     /// </summary>
     public void ShowResults()
     {

     }

     /// <summary>
     /// Calculates the result of the battle
     /// </summary>
     public void ProcessBattle()
     {

     }

     /// <summary>
     /// Called when a battle is won
     /// </summary>
     public void Win()
     {

     }

     /// <summary>
     /// Called when a battle is lost
     /// </summary>
     public void Lose()
     {

     }
}
