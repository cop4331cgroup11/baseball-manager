﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;


public class Trainer : MonoBehaviour {

    private Team m_objTeam;
    private User m_objUser;
     private MenuManager m_objMenuManager;
    private bool m_blnTraining;
    private DateTime m_DateTimeTrainEnd;
    private DateTime m_DateTimeTrainStart;
    private SocketIOComponent socket;
    private int intTrainValue;
    private int intExpGain;

    #region properties
    public bool IsTraining
    {
        get { return m_blnTraining; }
        set { m_blnTraining = value; }
    }

    public DateTime TrainEnd
    {
        get { return m_DateTimeTrainEnd; }
        set { m_DateTimeTrainEnd = value; }
    }

    public DateTime TrainStart
    {
        get { return m_DateTimeTrainStart; }
        set { m_DateTimeTrainStart = value; }
    }
    #endregion
     
    // Use this for initialization
    void Awake () {
        socket = GameObject.FindObjectOfType<SocketIOComponent>();
        m_objTeam = GameObject.FindObjectOfType<User>().ObjTeam;
        m_objUser = GameObject.FindObjectOfType<User>();
          m_objMenuManager = GameObject.FindObjectOfType<MenuManager>();
    }

    void Start()
    {
    }

    /// <summary>
    /// Starts Training of Shorter Time Period
    /// Then creates Json object and emits to server
    /// </summary>
    /// <param name="strTrainStat"></param>
    public void ShortTrain(string strTrainStat)
    {
        if (!m_objTeam.BlnIsTraining)
        {
               int intTime = 2;

               string strLeague = m_objUser.StrLeague;

               if (strLeague == "Pee Wee League") { intTime = 2; }
               else if (strLeague == "Little League") { intTime = 4; }
               else if (strLeague == "Junior League") { intTime = 6; }
               else if (strLeague == "Varsity League") { intTime = 8; }
               else if (strLeague == "Minor League") { intTime = 10; }
               else if (strLeague == "Major League") { intTime = 12; }

               m_DateTimeTrainStart = DateTime.Now;
            m_DateTimeTrainEnd = m_DateTimeTrainStart.AddMinutes(intTime);
            double trainingLengthMilliseconds = 15 * 1000; //2 * 60 * 1000; // 2 hours --> converted to minutes --> then converted to milliseconds
            intTrainValue = 1;
            intExpGain = 10;
            Dictionary<string, string> tempDict = new Dictionary<string, string> {
                                                                                {"username", m_objUser.StrUsername },
                                                                                { "trainingTimeLength", trainingLengthMilliseconds.ToString()},
                                                                                { "expGain", intExpGain.ToString()},
                                                                                { "stat", strTrainStat},
                                                                                { "statGain", intTrainValue.ToString()}
            };
            Debug.Log("Sending training to server for " + strTrainStat + "...");
            socket.Emit("START_TRAINING", new JSONObject(tempDict));
        }
        //boolean sent back from server for blnTraining
        //m_blnTraining = <fromServer>;
        m_objTeam.BlnIsTraining = true;
          m_objUser.UpdateLeague();
    }


    /// <summary>
    /// Starts Training of Longer Time Period
    /// Then creates Json object and emits to server
    /// </summary>
    /// <param name="strTrainStat"></param>
    public void LongTrain(string strTrainStat)
    {
        if (!m_objTeam.BlnIsTraining)
        {
               int intTime = 2;

               string strLeague = m_objUser.StrLeague;

               if (strLeague == "Pee Wee League") { intTime = 2; }
               else if (strLeague == "Little League") { intTime = 4; }
               else if (strLeague == "Junior League") { intTime = 6; }
               else if (strLeague == "Varsity League") { intTime = 8; }
               else if (strLeague == "Minor League") { intTime = 10; }
               else if (strLeague == "Major League") { intTime = 12; }

               m_DateTimeTrainStart = DateTime.Now;
            m_DateTimeTrainEnd = m_DateTimeTrainStart.AddHours(intTime);
            double trainingLengthMilliseconds = 15 * 1000; //2 * 60 * 1000; // 2 hours --> converted to minutes --> then converted to milliseconds
            intTrainValue = 10;
            intExpGain = 100;
            Dictionary<string, string> tempDict = new Dictionary<string, string> {
                                                                                {"username", m_objUser.StrUsername },
                                                                                { "trainingTimeLength", trainingLengthMilliseconds.ToString()},
                                                                                { "expGain", intExpGain.ToString()},
                                                                                { "stat", strTrainStat},
                                                                                { "statGain", intTrainValue.ToString()}
            };

            socket.Emit("START_TRAINING", new JSONObject(tempDict));
        }
        m_objTeam.BlnIsTraining = true;
          m_objUser.UpdateLeague();
    }


    /// <summary>
    /// Increases the Stats of each Player depending on the Stat trained
    /// </summary>
    /// <param name="strTrainStat"></param>
    /// <param name="intTrainValue"></param>
    public void TrainTeam(string strTrainStat, int intTrainValue)
    {
        int intMaxStat = 9999;
        switch (strTrainStat)
        {
            case "Pitching":
                {
                    foreach (var stat in m_objTeam.ListRoster)
                    {
                        stat.IntPitching += intTrainValue;
                        if(stat.IntPitching > intMaxStat)
                        {
                            stat.IntPitching = intMaxStat;
                        }
                    }
                    break;
                }

            case "Running":
                {
                    foreach (var stat in m_objTeam.ListRoster)
                    {
                        stat.IntRunning += intTrainValue;
                        if(stat.IntRunning > intMaxStat)
                        {
                            stat.IntRunning = intMaxStat;
                        }
                    }
                    break;
                }

            case "Fielding":
                {
                    foreach (var stat in m_objTeam.ListRoster)
                    {
                        stat.IntFielding += intTrainValue;
                        if(stat.IntFielding > intMaxStat)
                        {
                            stat.IntFielding = intMaxStat;
                        }
                    }
                    break;
                }
            case "Batting":
                {
                    foreach (var stat in m_objTeam.ListRoster)
                    {
                        stat.IntBatting += intTrainValue;
                        if(stat.IntBatting > intMaxStat)
                        {
                            stat.IntBatting = intMaxStat;
                        }
                    }
                    break;
                }
            default:
                {
                    break;
                }
        }
    }

    /// <summary>
    /// Accepts Json object from Server and Updates the values of the Players
    /// Sets Training Boolean to False
    /// </summary>
    /// <param name="intExp"></param>
    public void TrainComplete(JSONObject json)
    {
        Dictionary<string, string> dict = json.ToDictionary();

        m_objUser.intExp = int.Parse(dict["exp"]);

        List<Player> roster = new List<Player>();

        for (int i = 0; i < json["team"].Count; i++)
        {
            Dictionary<string, string> tempDict = json["team"][i].ToDictionary();
            
            Player tempPlayer = new Player(
                tempDict["name"],
                int.Parse(tempDict["spriteNum"]),
                int.Parse(tempDict["batting"]),
                int.Parse(tempDict["fielding"]),
                int.Parse(tempDict["pitching"]),
                int.Parse(tempDict["running"])
            );
            roster.Add(tempPlayer);
        }

        m_objTeam.ListRoster = roster;

        m_blnTraining = false;
          m_objMenuManager.ShowTrainingTime();
    }
}
