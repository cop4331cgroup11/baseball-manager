﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Schedule {

     private string[] m_arrSchedule;
     private string[] m_arrTeams = { "Thunder", "Timberwolves", "Spurs", "Yankees", "Phillies", "76ers", "Demons", "Angels", "Mavericks", "Heat", "Magic", "Suns", "Moons", "Stars", "Aliens", "Astros", "Marlons",
     "Red Sox", "White Sox", "Purple Sox", "Striped Sox", "Pink Sox", "Dodgers", "Cubs", "Giants", "Braves", "Royals", "Tigers", "Lions", "Patriots", "Raiders", "Pirates", "Vikings", "Marlins", "Brewers", "Rays",
     "Rockets", "Heroes", "Villains", "Spiders", "Predators", "Sharks", "Dolphins", "Jedis", "Ninjas", "Knicks", "Lakers", "Clippers", "Kings", "Pelicans", "Raptors", "Nuggets", "Hawks", "Eagles", "Falcons"};
     private int[] m_arrWinPercentage;
     private bool m_blnGameCompleted = false;

     #region Properties

     public string[] ArrSchedule
     {
          get { return m_arrSchedule; }
          set { m_arrSchedule = value; }
     }

     public int[] ArrWinPercent
     {
          get { return m_arrWinPercentage; }
          set { m_arrWinPercentage = value; }
     }

     public bool GameCompleted
     {
          get { return m_blnGameCompleted; }
          set { m_blnGameCompleted = value; }
     }

     #endregion

     // Use this for initialization
     public Schedule () {

          m_arrSchedule = new string[3];
          m_arrWinPercentage = new int[3];

          PopulateSchedule();
          PopulateWinPercents();
          

	}
	
	// Update is called once per frame
	void Update () {
	}

     /// <summary>
     /// Populates the initial schedule to be used
     /// </summary>
     void PopulateSchedule()
     {
          System.Random objRandom = new System.Random();

          for (int i = 0; i < m_arrSchedule.Length; i++)
          {
               m_arrSchedule[i] = m_arrTeams[objRandom.Next(m_arrTeams.Length-1)];
          }
     }

     /// <summary>
     /// Populates the initial win percentages to be used
     /// </summary>
     void PopulateWinPercents()
     {
          System.Random objRandom = new System.Random();

          for (int i = 0; i < m_arrWinPercentage.Length; i++)
          {
               m_arrWinPercentage[i] = objRandom.Next(100);
          }
     }

     /// <summary>
     /// Adjusts the schedule after a game is played to remove the team vsed from list and add a new one to the end
     /// </summary>
     public void AdjustAfterGame()
     {
          System.Random objRandom = new System.Random();

          m_arrSchedule[0] = m_arrSchedule[1];
          m_arrSchedule[1] = m_arrSchedule[2];
          m_arrSchedule[2] = m_arrTeams[objRandom.Next(m_arrTeams.Length-1)];

          m_arrWinPercentage[0] = m_arrWinPercentage[1];
          m_arrWinPercentage[1] = m_arrWinPercentage[2];
          m_arrWinPercentage[2] = objRandom.Next(100);

          m_blnGameCompleted = false;
     }
}
