﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

public class ChoosePlayer : MonoBehaviour {

    private SocketIOComponent socket;
    private User m_objUser;
    private LevelManager m_objLevelManager;
     private RandomManager m_objRandomManager;
     private System.Random rnd;

     void Awake()
    {
        socket = GameObject.FindObjectOfType<SocketIOComponent>();
        m_objUser = GameObject.FindObjectOfType<User>();
        m_objLevelManager = GameObject.FindObjectOfType<LevelManager>();
          m_objRandomManager = GameObject.FindObjectOfType<RandomManager>();

        RenderPotentialPlayers();
    }

    // Use this for initialization
    void Start () {
		
	}

    void RenderPotentialPlayers()
    {
        List<PlayerCard> objSpots = new List<PlayerCard>();

        List<Player> listPlayers = new List<Player>();        

        objSpots.Add(GameObject.Find("Big Card Template").GetComponent<PlayerCard>());
        objSpots.Add(GameObject.Find("Big Card Template 2").GetComponent<PlayerCard>());
        objSpots.Add(GameObject.Find("Big Card Template 3").GetComponent<PlayerCard>());

        for (int i = 0; i < listPlayers.Count; i++)
        {
            listPlayers[i] = new Player(rnd);
            PopulatePlayerCard(objSpots[i], listPlayers[i]);
        }
    }

    public void PopulatePlayerCard(PlayerCard objCard, Player objPlayer)
    {
        objCard.Player = objPlayer;
        objCard.gameObject.transform.GetChild(0).GetComponent<Text>().text = objPlayer.StrPlayerName;                                     // get player name
        objCard.gameObject.transform.GetChild(2).GetComponent<Image>().sprite = objPlayer.ObjPlayerSprite;                                // get player sprite
        objCard.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntBatting.ToString();       // get player batting
        objCard.gameObject.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntFielding.ToString();      // get player fielding
        objCard.gameObject.transform.GetChild(5).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntPitching.ToString();      // get player pitching
        objCard.gameObject.transform.GetChild(6).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntRunning.ToString();       // get player running
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
