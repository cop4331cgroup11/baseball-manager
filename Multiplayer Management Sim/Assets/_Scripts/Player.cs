﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player {

	private Sprite m_objPlayerSprite;
	private string m_strPlayerName;
     private int m_intPitching;
     private int m_intBatting;
     private int m_intFielding;
     private int m_intRunning;
     private int m_intSpriteNumber;
     private System.Random rnd;

     #region properties
     public Sprite ObjPlayerSprite
     {
          get { return m_objPlayerSprite; }
          set { m_objPlayerSprite = value; }
     }

     public string StrPlayerName
     {
          get { return m_strPlayerName; }
          set { m_strPlayerName = value; }
    }

     public int IntPitching
     { 
          get { return m_intPitching; }
          set { m_intPitching = value; }
     }

     public int IntBatting
     {
          get { return m_intBatting; }
          set { m_intBatting = value; }
     }

     public int IntFielding
     {
          get { return m_intFielding; }
          set { m_intFielding = value; }
     }

     public int IntRunning
     {
          get { return m_intRunning; }
          set { m_intRunning = value; }
     }

     public int IntSpriteNumber
     {
          get { return m_intSpriteNumber; }
          set { m_intSpriteNumber = value; }
     }

     #endregion

     // Use this for initialization

     //Will need Sprite
     public Player (System.Random Random) {

          rnd = Random;
        //rnd = new System.Random();

		//System.Random rnd = new System.Random();
		StrPlayerName = NameGenerator ();
		IntBatting = rnd.Next (1, 100);
		IntFielding = rnd.Next (1, 100);
		IntPitching = rnd.Next (1, 100);
		IntRunning = rnd.Next (1, 100);
     }

    public Player (string name, int spriteNum, int batting, int fielding, int pitching, int running)
    {
        this.StrPlayerName = name;
        this.IntSpriteNumber = spriteNum;
        this.IntBatting = batting;
        this.IntFielding = fielding;
        this.IntPitching = pitching;
        this.IntRunning = running;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
	
     /// <summary>
     /// creates player names like "Conmarbosh" or "Joalkev" or "Shildonjo"
     /// </summary>
     /// <returns></returns>
	private string NameGenerator () {
		//System.Random rnd = new System.Random ();
		string strPlayerName = "";
		string[] nameLibrary = { "CON", "MAR", "BOSH" , "JO", "AL", "KEV", "COD", "DYL", "BAM", "DON", "SHIL", "OTA", "ATA", "OLO", "PEPE"};

		for(int i = 0; i<3; i++){
			strPlayerName = strPlayerName + nameLibrary [rnd.Next (nameLibrary.Length)];
		}


		return ( char.ToUpper(strPlayerName[0]) + strPlayerName.Substring(1) );
	}

    public Dictionary<string, string> ToDictionary()
    {
        Dictionary<string, string> tempDict = new Dictionary<string, string>();

        tempDict["name"] = this.StrPlayerName;
        tempDict["spriteNum"] = this.IntSpriteNumber.ToString();
        tempDict["batting"] = this.IntBatting.ToString();
        tempDict["fielding"] = this.IntFielding.ToString();
        tempDict["pitching"] = this.IntPitching.ToString();
        tempDict["running"] = this.IntRunning.ToString();

        return tempDict;
    }
}


