﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;
using System.Text.RegularExpressions;

public class CreateAccount : MonoBehaviour {

    private SocketIOComponent socket;
    private User m_objUser;
    private LevelManager m_objLevelManager;

    void Awake()
    {
        socket = GameObject.FindObjectOfType<SocketIOComponent>();
        m_objUser = GameObject.FindObjectOfType<User>();
        m_objLevelManager = GameObject.FindObjectOfType<LevelManager>();
    }

    // Use this for initialization
    void Start () {
        socket.On("CREATE_ACCOUNT_SUCCESS", CreateAccSuccess);
        socket.On("CREATE_ACCOUNT_FAILED", CreateAccFailed);
        socket.On("GET_STATS_SUCCESS", GetStats);
    }

    public void CreateAcc()
    {
		var regexItem = new Regex("[^a-zA-Z0-9]");


        Dictionary<string, string> data = new Dictionary<string, string>();
        data["username"] = GameObject.FindGameObjectWithTag("TextBox").GetComponent<Text>().text;

		if(regexItem.IsMatch(data["username"])){
			
			return;
		}
        socket.Emit("CREATE_ACCOUNT", new JSONObject(data));
          //m_objLevelManager.LoadLevel("04a Player Selection");
     }

    public void CreateAccSuccess(SocketIOEvent e)
    {
        Debug.Log("Account Creation Success. Getting stats now...");
        socket.Emit("GET_STATS");

        m_objLevelManager.LoadLevel("04a Player Selection");
    }

    public void GetStats(SocketIOEvent e)
    {
        m_objUser.Login(e.data);
        Debug.Log("Stats recieved, and placed on user object.");

        m_objLevelManager.LoadLevel("04a Player Selection");
    }

    public void CreateAccFailed(SocketIOEvent e)
    {
        Debug.Log("Account Creation Failed.");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
