﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

     public AudioClip[] levelMusicChangeArray;

     private AudioSource m_objAudioSource;
     private int m_intCurrentLevel = 0;
     private int m_intPreviousSong = -1;

     #region Properties
     public float Volume
     {
          get { return m_objAudioSource.volume; }
          set { m_objAudioSource.volume = value; }
     }
     #endregion

     void Awake()
     {
          DontDestroyOnLoad(gameObject);
     }

     // Use this for initialization
     void Start () {
          m_objAudioSource = GetComponent<AudioSource>();
     }

     /// <summary>
     /// Checks every frame to see what music to play
     /// - returns if at splash screen
     /// - if the next song is the same as the previous song, do nothing
     /// - starts a random song in the array if no song is playing
     /// - if a song is already playing, do nothing
     /// </summary>
     void Update()
     {
          if (m_intCurrentLevel != 0)
          {

               int intSong = Random.Range(0, levelMusicChangeArray.Length);

               if (!m_objAudioSource.isPlaying && (m_intPreviousSong != intSong))
               {
                    AudioClip objThisLevelMusic = levelMusicChangeArray[intSong];

                    m_objAudioSource.clip = objThisLevelMusic;
                    m_objAudioSource.Play();
                    m_intPreviousSong = intSong;
               }
          }
     }

     void OnLevelWasLoaded(int intLevel)
     {
          m_intCurrentLevel = intLevel;
     }
}
