﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SocketIO;

public class MenuManager : MonoBehaviour {

     private SocketIOComponent socket;
     private User m_objUser;
     private Team m_objTeam;
    private LevelManager m_scriptLevelManager;
    private string m_strOppUsername;

     #region properties

     #endregion

     // Use this for initialization
     void Start () {
        m_objTeam = GameObject.FindObjectOfType<User>().ObjTeam;
        m_objUser = GameObject.FindObjectOfType<User>();
        m_scriptLevelManager = GameObject.FindObjectOfType<LevelManager>();
        socket = GameObject.FindObjectOfType<SocketIOComponent>();

        socket.On("GET_LEADERBOARD_INFO_SUCCESS", ShowLeaderboard);
        socket.On("FRIEND_BATTLE_REQUEST", HandleFriendBattleRequest);

          m_objUser.UpdateLeague();
          UpdateStats();

          if(SceneManager.GetActiveScene().name != "04a Player Selection")
          {
               ShowStatBar();
          }


        if (SceneManager.GetActiveScene().name == "03b Roster")
          {
               ShowRoster();
          }

          if (SceneManager.GetActiveScene().name == "03e Schedule")
          {
               ShowSchedule();
          }

          if (SceneManager.GetActiveScene().name == "04a Player Replacement")
          {
               ShowRosterWReplaceMent();
          }

          if(SceneManager.GetActiveScene().name == "03c Battle Type")
          {
               GameObject.Find("Friend Wrapper").transform.GetChild(0).gameObject.SetActive(false);
          }

          if(SceneManager.GetActiveScene().name == "03d Leaderboard")
          {
               RetrieveLeaderboardInfo();
          }

          if (m_objTeam.BlnIsTraining && SceneManager.GetActiveScene().name == "03a Tra-Inning")
          {
               ShowTrainingInProgess();
          }
          else if (!m_objTeam.BlnIsTraining == false && SceneManager.GetActiveScene().name == "03a Tra-Inning")  
          {
               ShowTrainingTime();
          }

          if(SceneManager.GetActiveScene().name == "03c Battle Type")
          {
               m_objUser.IntWinsBefore = m_objUser.intWins;
          }

          if(SceneManager.GetActiveScene().name == "03c Battle Result")
          {
               m_objUser.IntWinsAfter = m_objUser.intWins;
               if((m_objUser.IntWinsAfter - m_objUser.IntWinsBefore) == 0)
               {
                    BattleResult(false);
               }
               else
               {
                    BattleResult(true);
               }
          }
    }


     /// <summary>
     /// Populates the text of the gameobject with the correct battle result
     /// blnResult = true for win
     /// blnResult = false for lose
     /// </summary>
     /// <param name="blnResult"></param>
     public void BattleResult(bool blnResult)
     {
          if (blnResult)
          {
               GameObject.Find("Battle Result").GetComponent<Text>().text = "YOU WIN!!!";
          }
          else
          {
               GameObject.Find("Battle Result").GetComponent<Text>().text = "YOU LOSE!!";
               m_objUser.intExp -= 5;
               UpdateStats();
          }

     }
     
     public void HandleFriendBattleRequest(SocketIOEvent e) {
        OpenRequest(e.data["friendUsername"].ToString());
    }
    
    public void AcceptFriendBattle() {
          Dictionary<string, string> data = new Dictionary<string, string>();
          data["friendUsername"] = this.m_strOppUsername;// get the username of the friend that requested the battle be place the string here

          GameObject.Find("Request Wrapper").transform.GetChild(0).gameObject.SetActive(false);
          Debug.Log("accepting friend battle...");
          m_scriptLevelManager.LoadLevel("03c Battle Type");
          socket.Emit("START_FRIEND_BATTLE", new JSONObject(data));
     }

     public void DenyFriendBattle()
     {
          Dictionary<string, string> data = new Dictionary<string, string>();
          data["friendUsername"] = this.m_strOppUsername;// get the username of the friend that requested the battle be place the string here

          Debug.Log("set opponent to user: " + data["friendUsername"] + " by user: " + m_objUser.StrUsername);
          GameObject.Find("Request Wrapper").transform.GetChild(0).gameObject.SetActive(false);

          Debug.Log("sending server that were are declining friend battle...");
          socket.Emit("FRIEND_BATTLE_CANCELLED", new JSONObject(data));
     }

     /// <summary>
     /// Populates the leaderboard with the top players
     /// </summary>
     public void ShowLeaderboard(SocketIOEvent e)
     {
        //Debug.Log(e.data["leaderboard"].Count);
        // This creates an array of dictionaries (Dictionary<string, string>), 
        // that hold username and exp and is in descending order by exp
        // ex: leaderboard[0]["username"] --> "alextest", leaderboard[0]["exp"] --> "98" (note: exp is a string)
        List<Dictionary<string, string>> leaderboard = new List<Dictionary<string, string>>();
        for(int i = e.data["leaderboard"].Count - 1; i >= 0; i--)
        {
            leaderboard.Add(e.data["leaderboard"][i].ToDictionary());
        }
          //Debug.Log(leaderboard[0]["username"]);

          List<GameObject> listRanks = new List<GameObject>();

          listRanks.Add(GameObject.Find("Rank 1"));
          listRanks.Add(GameObject.Find("Rank 2"));
          listRanks.Add(GameObject.Find("Rank 3"));
          listRanks.Add(GameObject.Find("Rank 4"));
          listRanks.Add(GameObject.Find("Rank 5"));
          listRanks.Add(GameObject.Find("Rank 6"));
          listRanks.Add(GameObject.Find("Rank 7"));
          listRanks.Add(GameObject.Find("Rank 8"));
          listRanks.Add(GameObject.Find("Rank 9"));

          for(int i = 0; i < listRanks.Count && i < leaderboard.Count; i++)
          {
               listRanks[i].transform.GetChild(1).GetComponent<Text>().text = leaderboard[i]["username"];
               listRanks[i].transform.GetChild(2).GetComponent<Text>().text = leaderboard[i]["exp"];
          }

          int j = -1;

          // Search for yourself
          for(int i = 0; i < leaderboard.Count; i++)
          {
               if(leaderboard[i]["username"] == m_objUser.StrUsername)
               {
                    j = i;
               }
          }

          GameObject objTemp = GameObject.Find("Rank User");
          objTemp.transform.GetChild(0).GetComponent<Text>().text = (j+1).ToString();
          objTemp.transform.GetChild(1).GetComponent<Text>().text = leaderboard[j]["username"];
          objTemp.transform.GetChild(2).GetComponent<Text>().text = leaderboard[j]["exp"];

     }

     public void RetrieveLeaderboardInfo()
    {
        Debug.Log("requesting leaderboard info...");
        socket.Emit("GET_LEADERBOARD_INFO");
    }

     /// <summary>
     /// Populates the stat bar at the top of the screen with the appropriate stats
     /// </summary>
     public void ShowStatBar()
     {

        GameObject.FindGameObjectWithTag("LeagueName").GetComponent<Text>().text = m_objUser.StrLeague;
        GameObject.FindGameObjectWithTag("CurEnergy").GetComponent<Text>().text = m_objUser.intEnergy.ToString();
        GameObject.FindGameObjectWithTag("MaxEnergy").GetComponent<Text>().text = m_objUser.intMaxEnergy.ToString();
        GameObject.FindGameObjectWithTag("Exp").GetComponent<Text>().text = m_objUser.intExp.ToString();
        GameObject.FindGameObjectWithTag("Wins").GetComponent<Text>().text = m_objUser.intWins.ToString();
        GameObject.FindGameObjectWithTag("Losses").GetComponent<Text>().text = m_objUser.intLosses.ToString();
          GameObject.Find("Energy").GetComponent<Slider>().value = ((float) m_objUser.intEnergy / (float) m_objUser.intMaxEnergy);
          GameObject.Find("Experience").GetComponent<Slider>().value = ExpBarPercent();
    }

     /// <summary>
     /// Used to get percentage that xp bar is filled
     /// </summary>
     float ExpBarPercent()
     {
          int intExp = m_objUser.intExp;
          string strLeague = m_objUser.StrLeague;
          float decPercent;

          if (strLeague == "Pee Wee League") { decPercent = (float) intExp / 100f; }
          else if (strLeague == "Little League") { decPercent = (float) intExp / 1000f; }
          else if (strLeague == "Junior League") { decPercent = (float) intExp / 10000f; }
          else if (strLeague == "Varsity League") { decPercent = (float) intExp / 100000f; }
          else if (strLeague == "Minor League") { decPercent = (float) intExp / 1000000f; }
          else { decPercent = 1; }

          return decPercent;
     }

     #region Roster

     /// <summary>
     /// Populates gameobjects with the appropriate players in the team
     /// </summary>
     public void ShowRoster()
     {
          SpriteManager objSpriteManager = GameObject.FindObjectOfType<SpriteManager>();
          List<PlayerCard> objSpots = new List<PlayerCard>();
          PlayerCard objBigCard = GameObject.Find("Big Card Template").GetComponent<PlayerCard>();


          objSpots.Add(GameObject.Find("Card Template").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (1)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (2)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (3)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (4)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (5)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (6)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (7)").GetComponent<PlayerCard>());
          objSpots.Add(GameObject.Find("Card Template (8)").GetComponent<PlayerCard>());
          

          for(int i = 0; i < m_objUser.ObjTeam.ListRoster.Count; i++)
          {
               PopulatePlayerCard(objSpots[i], m_objTeam.ListRoster[i]);
          }

          if (!(m_objTeam.SelectedPlayer == null))
          {
               PopulatePlayerCard(objBigCard, m_objTeam.SelectedPlayer);
          }
          else
          {
               objBigCard.gameObject.transform.GetChild(0).GetComponent<Text>().text = "???";                                               // get player name
               objBigCard.gameObject.transform.GetChild(2).GetComponent<Image>().sprite = objSpriteManager.emptyCard;                       // get player sprite
               objBigCard.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = "???";                         // get player batting
               objBigCard.gameObject.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = "???";                         // get player fielding
               objBigCard.gameObject.transform.GetChild(5).transform.GetChild(0).GetComponent<Text>().text = "???";                         // get player pitching
               objBigCard.gameObject.transform.GetChild(6).transform.GetChild(0).GetComponent<Text>().text = "???";                         // get player running
          }
     }

     /// <summary>
     /// Populates the stats on the playercard gameobject
     /// </summary>
     /// <param name="objPlayer"></param>
     public void PopulatePlayerCard(PlayerCard objCard, Player objPlayer)
     {
          objCard.Player = objPlayer;
          objCard.gameObject.transform.GetChild(0).GetComponent<Text>().text = objPlayer.StrPlayerName;                                     // get player name
          objCard.gameObject.transform.GetChild(2).GetComponent<Image>().sprite = objCard.ObjSpriteManager.playerImages[objPlayer.IntSpriteNumber];                                // get player sprite
          objCard.gameObject.transform.GetChild(3).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntBatting.ToString();       // get player batting
          objCard.gameObject.transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntFielding.ToString();      // get player fielding
          objCard.gameObject.transform.GetChild(5).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntPitching.ToString();      // get player pitching
          objCard.gameObject.transform.GetChild(6).transform.GetChild(0).GetComponent<Text>().text = objPlayer.IntRunning.ToString();       // get player running
     }

     /// <summary>
     /// Swaps the batting order of the previously selected player and the player that is clicked on
     /// </summary>
     /// <param name="objPlayer"></param>
     public void SwapBattingOrder(PlayerCard objPlayer)
     {
          if(m_objTeam.SelectedPlayer == null)
          {
               m_objTeam.SelectedPlayer = objPlayer.Player;
               ShowRoster();
          }
          else
          {
               int i = m_objTeam.ListRoster.IndexOf(m_objTeam.SelectedPlayer);
               int j = m_objTeam.ListRoster.IndexOf(objPlayer.Player);
               Player objTempPlayer = objPlayer.Player;
               m_objTeam.ListRoster[j] = m_objTeam.ListRoster[i];
               m_objTeam.ListRoster[i] = objTempPlayer;
               m_objTeam.SelectedPlayer = null;
               ShowRoster();
          }
     }

     /// <summary>
     /// Swaps the batting order of the previously selected player and the player that is clicked on (also works for adding
     /// a player to team)
     /// </summary>
     /// <param name="objPlayer"></param>
     public void SwapBattingOrderWTempPlayer(PlayerCard objPlayer)
     {
          int i;
          int j;
          Player objTempPlayer;

          if (m_objTeam.SelectedPlayer == null)
          {
               m_objTeam.SelectedPlayer = objPlayer.Player;
               ShowRosterWReplaceMent();
          }
          else if(objPlayer.Player == m_objTeam.SelectedPlayer)
          {
               m_objTeam.SelectedPlayer = null;
               ShowRosterWReplaceMent();
          }
          else
          {
               if(m_objTeam.SelectedPlayer == m_objTeam.TempPlayer)
               {
                    i = m_objTeam.ListRoster.IndexOf(objPlayer.Player);
                    objTempPlayer = m_objTeam.ListRoster[i];

                    m_objTeam.ListRoster[i] = m_objTeam.TempPlayer;
                    m_objTeam.TempPlayer = objTempPlayer;
                    m_objTeam.SelectedPlayer = null;

               }
               else if(objPlayer.Player == m_objTeam.TempPlayer)
               {
                    i = m_objTeam.ListRoster.IndexOf(m_objTeam.SelectedPlayer);
                    objTempPlayer = m_objTeam.ListRoster[i];

                    m_objTeam.ListRoster[i] = m_objTeam.TempPlayer;
                    m_objTeam.TempPlayer = objTempPlayer;
                    m_objTeam.SelectedPlayer = null;
               }
               else
               {
                    i = m_objTeam.ListRoster.IndexOf(m_objTeam.SelectedPlayer);
                    j = m_objTeam.ListRoster.IndexOf(objPlayer.Player);
                    objTempPlayer = objPlayer.Player;

                    m_objTeam.ListRoster[j] = m_objTeam.ListRoster[i];
                    m_objTeam.ListRoster[i] = objTempPlayer;
                    m_objTeam.SelectedPlayer = null;
               }

               ShowRosterWReplaceMent();
          }
     }

     /// <summary>
     /// Resets the selected player in team
     /// </summary>
     public void UnselectPlayer()
     {
          m_objTeam.SelectedPlayer = null;
     }

     /// <summary>
     /// Populates gameobjects with appropriate players in team & the player that is about to be added
     /// </summary>
     public void ShowRosterWReplaceMent()
     {
          ShowRoster();

          PlayerCard objTemp = GameObject.Find("Card Template (9)").GetComponent<PlayerCard>();
          PopulatePlayerCard(objTemp, m_objTeam.TempPlayer);
     }

     #endregion

     #region schedule

     /// <summary>
     /// Populates the schedule with a list of the next few randomized teams to be
     /// battled against
     /// </summary>
     public void ShowSchedule()
     {
          GameObject objGame1 = GameObject.Find("1 Game Away");
          GameObject objGame2 = GameObject.Find("2 Games Away");
          GameObject objGame3 = GameObject.Find("3 Games Away");

          objGame1.transform.GetChild(0).GetComponent<Text>().text = m_objUser.Schedule.ArrSchedule[0];
          objGame1.transform.GetChild(2).GetComponent<Text>().text = "Skill Level:";
          objGame1.transform.GetChild(3).GetComponent<Text>().text = m_objUser.Schedule.ArrWinPercent[0].ToString() + "%" ;

          objGame2.transform.GetChild(0).GetComponent<Text>().text = m_objUser.Schedule.ArrSchedule[1];
          objGame2.transform.GetChild(2).GetComponent<Text>().text = "Skill Level:";
          objGame2.transform.GetChild(3).GetComponent<Text>().text = m_objUser.Schedule.ArrWinPercent[1].ToString() + "%";

          objGame3.transform.GetChild(0).GetComponent<Text>().text = m_objUser.Schedule.ArrSchedule[2];
          objGame3.transform.GetChild(2).GetComponent<Text>().text = "Skill Level:";
          objGame3.transform.GetChild(3).GetComponent<Text>().text = m_objUser.Schedule.ArrWinPercent[2].ToString() + "%";

     }

     /// <summary>
     /// Calculates the battle against a team in the schedule
     /// </summary>
     public void CpuBattle()
     {
          int intWinPercent = m_objUser.Schedule.ArrWinPercent[0];
          int intUserTotal = UserTotal();
          int intOpponentTotal = OpponentTotal(intWinPercent);

          if(m_objUser.Schedule.GameCompleted == false)
          {
               CalculateCpuBattle(intUserTotal, intOpponentTotal);
          }
          

     }

     /// <summary>
     /// Gets the stat total of the players on the users team
     /// </summary>
     int UserTotal()
     {
          int intTotal = 0;

          for (int i = 0; i < m_objTeam.ListRoster.Count; i++)
          {
               intTotal += m_objTeam.ListRoster[i].IntBatting;
               intTotal += m_objTeam.ListRoster[i].IntFielding;
               intTotal += m_objTeam.ListRoster[i].IntPitching;
               intTotal += m_objTeam.ListRoster[i].IntRunning;
          }

          return intTotal;
     }

     /// <summary>
     /// Gets the stat total of the randomized opponent's players
     /// </summary>
     /// <returns></returns>
     int OpponentTotal(int intWinPercent)
     {
          int intLowRand = 0;
          int intHighRand = 0;
          int intTotal = 0;
          float decWinPercent = ((float) intWinPercent / 100f) + .5f;
          System.Random objRandom = new System.Random();

          string strLeague = m_objUser.StrLeague;

          if (strLeague == "Pee Wee League") { intLowRand = 1; intHighRand = 100; }
          else if (strLeague == "Little League") { intLowRand = 51; intHighRand = 250; }
          else if (strLeague == "Junior League") { intLowRand = 251; intHighRand = 1000; }
          else if (strLeague == "Varsity League") { intLowRand = 1001; intHighRand = 2500; }
          else if (strLeague == "Minor League") { intLowRand = 2501; intHighRand = 6000; }
          else if (strLeague == "Major League") { intLowRand = 6001; intHighRand = 9999; }

          for(int i = 0; i < 36; i++)
          {
               intTotal += objRandom.Next(intLowRand, intHighRand);
          }



          return (int)(intTotal * decWinPercent);
     }

     void CalculateCpuBattle(int intUserTotal, int intCpuTotal)
     {
          int intUserScore = 0;
          int intCpuScore = 0;
          int intUserCopy;
          int intCpuCopy;
          int intUserMultiplier;
          int intCpuMultiplier;
          string strResult;
          System.Random objRandom = new System.Random();

          for(int i = 0; i < 20; i++)
          {
               intUserMultiplier = objRandom.Next(1, 100);
               intCpuMultiplier = objRandom.Next(1, 100);
               intUserCopy = intUserTotal * intUserMultiplier;
               intCpuCopy = intCpuTotal * intCpuMultiplier;
               
               if(intUserMultiplier > 25 && intCpuMultiplier > 25)
               {
                    if(intUserCopy > intCpuCopy)
                    {
                         intUserScore++;
                    }
                    else if(intCpuCopy > intUserCopy)
                    {
                         intCpuScore++;
                    }
               }
          }

          if(intCpuScore > intUserScore) { strResult = "Loss"; }
          else if(intUserScore > intCpuScore) { strResult = "Win"; m_objUser.intExp++; }
          else { strResult = "Tie"; }

          m_objUser.Schedule.GameCompleted = true;

          GameObject.FindGameObjectWithTag("WinRate").GetComponent<Text>().text = (intUserScore.ToString() + " - " + intCpuScore.ToString());
          GameObject.FindGameObjectWithTag("Win%").GetComponent<Text>().text = strResult;


     }

     public void AdvanceToNextGame()
     {
          if(m_objUser.Schedule.GameCompleted == true)
          {
               m_objUser.Schedule.AdjustAfterGame();
               ShowSchedule();
               m_objUser.UpdateLeague();
               UpdateStats();
               ShowStatBar();
               
          }

     }

     #endregion

     /// <summary>
     /// Show text box to challenge a friend
     /// </summary>
     public void ShowBattleFriendTextBox()
     {
          GameObject.Find("Friend Wrapper").transform.GetChild(0).gameObject.SetActive(true);
     }


     public void HideBattleFriendTextBox()
     {
          GameObject.Find("Friend Wrapper").transform.GetChild(0).gameObject.SetActive(false);
     }

     /// <summary>
     /// Spawns a confirm battle box when another user sends a challenge
     /// </summary>
     public void ConfirmBattle()
     {

     }

     /// <summary>
     /// Shows that a stat is already being trained
     /// </summary>
     public void ShowTrainingInProgess()
     {
          GameObject[] arrLightTraining = GameObject.FindGameObjectsWithTag("LightTraining");
          GameObject[] arrHeavyTraining = GameObject.FindGameObjectsWithTag("HeavyTraining");

          for(int i = 0; i < arrLightTraining.Length; i++)
          {
               arrLightTraining[i].GetComponent<Text>().text = "In Progress";
               arrHeavyTraining[i].GetComponent<Text>().text = "In Progress";

          }
     }

     public void ShowTrainingTime()
     {
          Debug.Log(m_objTeam.BlnIsTraining);
          string strTemp = "";
          int intTime = 2;

          string strLeague = m_objUser.StrLeague;

          if(strLeague == "Pee Wee League") { intTime = 2; }
          else if(strLeague == "Little League") { intTime = 4; }
          else if (strLeague == "Junior League") { intTime = 6; }
          else if (strLeague == "Varsity League") { intTime = 8; }
          else if (strLeague == "Minor League") { intTime = 10; }
          else if (strLeague == "Major League") { intTime = 12; }

          strTemp += intTime.ToString();

          GameObject[] arrLightTraining = GameObject.FindGameObjectsWithTag("LightTraining");
          GameObject[] arrHeavyTraining = GameObject.FindGameObjectsWithTag("HeavyTraining");

          for (int i = 0; i < arrLightTraining.Length; i++)
          {
               arrLightTraining[i].GetComponent<Text>().text = strTemp + " MINUTE";
               arrHeavyTraining[i].GetComponent<Text>().text = strTemp + " HOUR";

          }
          
     }

     public void UpdateTeam()
    {
        Debug.Log("Attempting to update team...");
        Dictionary<string, JSONObject> data = new Dictionary<string, JSONObject>();
        JSONObject[] roster = new JSONObject[9];
        for(int i = 0; i < m_objTeam.ListRoster.Count; i++)
        {
            roster[i] = new JSONObject(m_objTeam.ListRoster[i].ToDictionary());
        }
        data["team"] = new JSONObject(roster);
        Debug.Log("Sending server new team: " + new JSONObject(data));
        socket.Emit("UPDATE_TEAM", new JSONObject(data));
    }

     public void UpdateStats()
     {
          Debug.Log("Attempting to update stats...");
          Dictionary<string, string> data = new Dictionary<string, string>();

          data["wins"] = m_objUser.intWins.ToString();        // Wins
          data["losses"] = m_objUser.intLosses.ToString();    // Losses
          data["exp"] = m_objUser.intExp.ToString();          // Exp
          data["energy"] = m_objUser.intEnergy.ToString();    // Energy
          data["league"] = m_objUser.StrLeague;               // League 

          Debug.Log("Sending sever new stats: " + new JSONObject(data));
          socket.Emit("UPDATE_STATS", new JSONObject(data));
          
     }

     #region popups

     public void ClosePopup()
     {
          GameObject.Find("Popup Wrapper").transform.GetChild(0).gameObject.SetActive(false);
     }

     /// <summary>
     ///  for when you tried to request a battle with another user
     /// </summary>
     /// <param name="strMessage"></param>
     public void OpenPopup(string strMessage)
     {
          GameObject.Find("Popup Wrapper").transform.GetChild(0).gameObject.SetActive(true);
          GameObject.Find("Popup Wrapper").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = strMessage;
     }


     /// <summary>
     /// For when another user requests to battle you
     /// </summary>
     /// <param name="strUsername"></param>
     public void OpenRequest(string strUsername)
     {
          GameObject.Find("Request Wrapper").transform.GetChild(0).gameObject.SetActive(true);
          GameObject.Find("Request Wrapper").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = strUsername.ToString() + " WANTS TO BATTLE YOU! DO YOU ACCEPT?";

          this.m_strOppUsername = strUsername.Substring(1, strUsername.Length - 2);
     }

     #endregion popups
}
