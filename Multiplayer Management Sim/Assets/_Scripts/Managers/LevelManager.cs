﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

     public float autoLoadNextLevelAfter;

     void Start()
     {
          if (autoLoadNextLevelAfter > 0)
          {
               Invoke("LoadNextLevel", autoLoadNextLevelAfter);
          }

     }

     public void LoadLevel(string strName)
     {
          SceneManager.LoadScene(strName);
     }

     public void QuitRequest()
     {
          Debug.Log("Quit game requested.");
          Application.Quit();
     }

     public void LoadNextLevel()
     {
          SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
     }
}
