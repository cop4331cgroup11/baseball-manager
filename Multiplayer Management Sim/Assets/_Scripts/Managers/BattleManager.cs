﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour {

    private SocketIOComponent socket;
    private User m_scriptUser;
    private LevelManager m_scriptLevelManager;
     private MenuManager m_objMenuManager;

    void Awake()
    {
        socket = GameObject.FindObjectOfType<SocketIOComponent>();
        m_scriptUser = GameObject.FindObjectOfType<User>();
        m_scriptLevelManager = GameObject.FindObjectOfType<LevelManager>();
          m_objMenuManager = GameObject.FindObjectOfType<MenuManager>();
    }

    // Use this for initialization
    void Start () {
        
        if(SceneManager.GetActiveScene().name == "03c Pre Battle")
        {
            RenderPreGameInfo();
        } else if (SceneManager.GetActiveScene().name == "03c Battle")
        {
            RenderBattleResults();
        }
        
        socket.On("BATTLE_RESULTS", BattleResults);
        socket.On("PRE_BATTLE_INFO", OpponentInfo);
        socket.On("FRIEND_BATTLE_CANCELLED", HandleFriendBattleCancellation);
        socket.On("FRIEND_OFFLINE", HandleFriendNotOnline);
	}

    public void StartFriendBattle()
    {
          if (m_scriptUser.intEnergy > 0)
          {
               Dictionary<string, string> data = new Dictionary<string, string>();
               data["friendUsername"] = GameObject.Find("Friend Wrapper").transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).GetComponent<Text>().text;
               GameObject.Find("Friend Wrapper").transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).GetComponent<Text>().text = "...";
               
               if(data["friendUsername"] == m_scriptUser.StrUsername)
               {
                GameObject.Find("Friend Wrapper").transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).GetComponent<Text>().text = "invalid...";
               } else
               {
                Debug.Log("Finding requested friend to battle...");
                socket.Emit("LOOK_FOR_FRIEND", new JSONObject(data));
               }
          }
    }

     public void StartBattle()
     {
          if (m_scriptUser.intEnergy > 0) { 
               Dictionary<string, string> data = new Dictionary<string, string>();
               data["league"] = m_scriptUser.StrLeague;

               Debug.Log("Finding player to battle...");
               socket.Emit("START_BATTLE", new JSONObject(data));
          }
    }
    
    public void HandleFriendBattleCancellation(SocketIOEvent e) {
        // this is where you present to the user that the friend he requested has denied the challenge
        // so let the user know this...
        Debug.Log("friend cancelled battle.");
        m_objMenuManager.OpenPopup("YOUR FRIEND DOESN'T WANT TO BATTLE YOU.");
    }
    
    public void HandleFriendNotOnline(SocketIOEvent e) {
        // this is where you present to the user that the friend he requested is not online
        // so let the user know this...
        Debug.Log("friend accepted battle.");
        m_objMenuManager.OpenPopup("YOUR FRIEND ISN'T ONLINE.");
     }

    void BattleResults(SocketIOEvent e)
    {
        Debug.Log("Battle Results recieved");
        //Debug.Log(e.data);
        Dictionary<string, string> json = e.data["battleResults"]["user"].ToDictionary();
        Dictionary<string, int> userBattleResults = new Dictionary<string, int>();
        foreach (var item in json)
        {
            userBattleResults[item.Key] = int.Parse(item.Value);
        }
        m_scriptUser.ObjBattle.DictUserResults = userBattleResults;

        json = e.data["battleResults"]["opponent"].ToDictionary();
        Dictionary<string, int> oppBattleResults = new Dictionary<string, int>();
        foreach (var item in json)
        {
            oppBattleResults[item.Key] = int.Parse(item.Value);
        }
        m_scriptUser.ObjBattle.DictOpponentResults = oppBattleResults;

        m_scriptUser.intEnergy = int.Parse(e.data["energy"].ToString());
        m_scriptUser.intWins = int.Parse(e.data["wins"].ToString());
        m_scriptUser.intLosses = int.Parse(e.data["losses"].ToString());
        Debug.Log("user's new energy: " + m_scriptUser.intEnergy);
        Debug.Log("user's new wins and lossess : " + m_scriptUser.intWins + ' ' + m_scriptUser.intLosses);
    }

    void OpponentInfo(SocketIOEvent e)
    {
        Debug.Log("Opponent retrieved, Opponent: " + e.data.ToDictionary()["username"]);

        m_scriptUser.SetOpponent(e.data);

        //GameObject.Find("Friend Wrapper").transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).GetComponent<Text>().text = "sucess...";

        m_scriptLevelManager.LoadLevel("03c Pre Battle");
    }

    void RenderPreGameInfo()
    {
        Debug.Log("Entering Rendering of pre battle info");
        GameObject.FindGameObjectWithTag("BattleUser").GetComponent<Text>().text = m_scriptUser.StrUsername;
        GameObject.FindGameObjectWithTag("BattleOpponent").GetComponent<Text>().text = m_scriptUser.ObjBattle.StrOpponentUsername;
    }

    void RenderBattleResults()
    {
        Debug.Log("Entering Rendering of battle results");
        GameObject.FindGameObjectWithTag("BattleUser").GetComponent<Text>().text = m_scriptUser.StrUsername;
        GameObject.FindGameObjectWithTag("BattleOpponent").GetComponent<Text>().text = m_scriptUser.ObjBattle.StrOpponentUsername;

        GameObject.Find("Inning 1").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning1"].ToString();
        GameObject.Find("Inning 1").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning1"].ToString();
        GameObject.Find("Inning 2").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning2"].ToString();
        GameObject.Find("Inning 2").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning2"].ToString();
        GameObject.Find("Inning 3").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning3"].ToString();
        GameObject.Find("Inning 3").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning3"].ToString();
        GameObject.Find("Inning 4").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning4"].ToString();
        GameObject.Find("Inning 4").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning4"].ToString();
        GameObject.Find("Inning 5").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning5"].ToString();
        GameObject.Find("Inning 5").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning5"].ToString();
        GameObject.Find("Inning 6").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning6"].ToString();
        GameObject.Find("Inning 6").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning6"].ToString();
        GameObject.Find("Inning 7").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning7"].ToString();
        GameObject.Find("Inning 7").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning7"].ToString();
        GameObject.Find("Inning 8").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning8"].ToString();
        GameObject.Find("Inning 8").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning8"].ToString();
        GameObject.Find("Inning 9").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["inning9"].ToString();
        GameObject.Find("Inning 9").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["inning9"].ToString();
        GameObject.Find("Total").transform.GetChild(0).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictUserResults["total"].ToString();
        GameObject.Find("Total").transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = m_scriptUser.ObjBattle.DictOpponentResults["total"].ToString();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
