﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomManager : MonoBehaviour {

     private System.Random m_objRandom;

     #region Properties
     public System.Random objRandom
     {
          get { return m_objRandom; }
          set { m_objRandom = value; }
     }
     #endregion

     void Awake()
     {
          DontDestroyOnLoad(gameObject);
     }

	// Use this for initialization
	void Start () {
          m_objRandom = new System.Random();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
