﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocketIO;

public class MainScreen : MonoBehaviour {

    private SocketIOComponent socket;
    private User m_scriptUser;
    private LevelManager m_scriptLevelManager;

    void Awake()
    {
        socket = GameObject.FindObjectOfType<SocketIOComponent>();
        m_scriptUser = GameObject.FindObjectOfType<User>();
        m_scriptLevelManager = GameObject.FindObjectOfType<LevelManager>();
    }

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
		
	}
}
