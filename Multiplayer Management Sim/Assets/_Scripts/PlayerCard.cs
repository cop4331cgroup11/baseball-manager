﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCard : MonoBehaviour {

     private Player m_objPlayer;
     private SpriteManager m_objSpriteManager;
     private RandomManager m_objRandomManager;
     private System.Random rnd;

     #region properties

     public Player Player
     {
          get { return m_objPlayer; }
          set { m_objPlayer = value; }
     }

     public Sprite ObjPlayerSprite
     {
          get { return m_objPlayer.ObjPlayerSprite; }
          set { m_objPlayer.ObjPlayerSprite = value; }
     }

     public string StrPlayerName
     {
          get { return m_objPlayer.StrPlayerName; }
          set { m_objPlayer.StrPlayerName = value; }
     }

     public int IntPitching
     {
          get { return m_objPlayer.IntPitching; }
          set { m_objPlayer.IntPitching = value; }
     }

     public int IntBatting
     {
          get { return m_objPlayer.IntBatting; }
          set { m_objPlayer.IntBatting = value; }
     }

     public int IntFielding
     {
          get { return m_objPlayer.IntFielding; }
          set { m_objPlayer.IntFielding = value; }
     }

     public int IntRunning
     {
          get { return m_objPlayer.IntRunning; }
          set { m_objPlayer.IntRunning = value; }
     }

     public int IntSpriteNumber
     {
          get { return m_objPlayer.IntSpriteNumber; }
          set { m_objPlayer.IntSpriteNumber = value; }
     }

    public SpriteManager ObjSpriteManager
    {
        get
        {
            return m_objSpriteManager;
        }

        set
        {
            m_objSpriteManager = value;
        }
    }
    #endregion

    // Use this for initialization
    void Awake () {
          ObjSpriteManager = GameObject.FindObjectOfType<SpriteManager>();
          m_objRandomManager = GameObject.FindObjectOfType<RandomManager>();
          rnd = m_objRandomManager.objRandom;

          m_objPlayer = new Player(rnd);
          m_objPlayer.IntSpriteNumber = rnd.Next(ObjSpriteManager.playerImages.Length);
          m_objPlayer.ObjPlayerSprite = ObjSpriteManager.playerImages[m_objPlayer.IntSpriteNumber];
	}
	
	// Update is called once per frame
	void Update () {
		
	}

     public void UpdateSprite()
     {
          m_objPlayer.ObjPlayerSprite = ObjSpriteManager.playerImages[m_objPlayer.IntSpriteNumber];
     }
}
