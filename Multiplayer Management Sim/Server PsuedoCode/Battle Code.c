//tracks what player is one what base
Object Bases{
	byte Bases = 0000;
	//pushes a bit, returns Most Significant bit, represents moving 1 base.
	int Push(int Bit) {
		Bases = Bases << 1;
		Bases = Bases + Bit;
		return MSB();	
	}
	
	//returns most significant digit
	int MSB(){
		byte tBases = Bases;
		tBases = tBases & 1000;
		tBases = tBases >> 3;
		return tBases;
	}
	
	//empty the base
	void resetBases() Bases = 0000;
}

//tracks score and who's turn it is.
Object Score{
	int team1Score;
	int team2Score;
	boolean turn;
	Score(){
		team1Score = 0;
		team2Score = 0;
		turn = 0;
	}
	
	Increase(int n){
		if(turn) team2Score+=n;
		else team1Score+=n;
	}
	
	Swap(){
		turn = !turn;
	}
	
}
<<<<<<< HEAD
Score Game(Team A, Team B) { 
	Global Score();
	Global Bases();
	for(int i = 0; i < 9; i++) Inning(A, B);
	return Score;
=======
void Game(Team A, Team B) { 
	for(int i = 0; i < 9; i++) Inning(A, B);
>>>>>>> origin/master
}

//innings, plays top half, then bottom half.
void Inning(Team A, Team B) {
	int OutCounter = 0;
	int i = 0;
	
	while(OutCounter < 3){
		if(BaseCalc(A.get(i), B) == 1) OutCounter++;
		i++;
		if(i==9) i=0;
	}
	Score.Swap();
	OutCounter = 0;
	i=0;
	while(OutCounter < 3) {
		if(BaseCalc(B.get(i), A) == 1) OutCounter++;
		i++;
		if(i==9) i=0;
	}
} 


//Stages one encounter between a player, pitcher, and the field.
//updates the score based on how many people are on base.
//return 0 if successful bat, 1 if out.
int BaseCalc(Player O, Team D){
	Player TempPitcher = Team.GetPitcher();
	if(Bat(O, TempPitcher)){		//three at bats
		Player TempDPlayer = Team.GetRandomPlayer();
		int tmpN = MoveBase(CompareStates(O.getRun(), TempDPlayer.getFLD)
		if(tmpN == 0) return 1;
		Score.Increase(tmpN);	
		return 0;
	} 	
	return 1;
}

//checks if player passes any of three pitches
Boolean Bat(Player B, Player P){
	for(int i = 0; i<3; i++){
		if(CompareStats(B.getBat(), P.getPit()) > 0) return true;
	}
	return false;
}

//if >0, A wins, else, B wins
int CompareStats (int A, int B){
	int rng (0-100) - makes a number between zero and 100
	int tA = rng - A 
	int tB = rng - B

	if(tA < tB) return 0;
	else if(tA-tB < 10) return 1;
	else if(tA-tB < 25) return 2;
	else if(tA-tB < 50) return 3;
	else return 4;
}


//returns number of people reaching home.
//n = number of bases made by Runner
int MoveBase(int n){
	if(n==0) return 0;
	int score=0;
	score+=Bases.Push(1);
	n--;
	for(; n>0; n--){
		n--;
		score+=Bases.Push(0);
	}
	return score;
}